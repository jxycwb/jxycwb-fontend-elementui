import request from '@/utils/request'
import SERVER from '../../server'

// 获取用户信息
export const getUsersList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees`
  return request.get(url, {
    params: params
  })
}

// 获取用户信息
export const getEmployeeNo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/employees/employeeNo`
  return request.get(url, {
    params: params
  })
}

// 新增用户信息
export const addUserInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees`
  return request.post(url, params)
}
// 根据id获取用户信息
export const getUserInfoById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/${id}`
  return request.get(url, {
    id: Number(id)
  })
}

// 删除用户信息
export const delUserInfo = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/delete`
  return request.get(url, {
    params: { id }
  })
}

//
export const findTcCodeByType = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/findTcCodeByType/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
// 查询所有的岗位信息
export const findEmpRoles = (dealerCode) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/${dealerCode}/roles`
  return request.get(url)
}

//
export const findEmployeeRoleById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/findEmployeeRoleById/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
// 编辑用户信息
export const editUserInfo = (params = {}, id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/${id}`
  return request.put(url, params)
}
// 获取职务信息
export const getPositionType = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/positions/duty/dicts`
  return request.get(url, {
    params: params
  })
}
// 获取班组信息
export const getWorkGroup = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/workgroups/dicts/select`
  return request.get(url, {
    params: params
  })
}
// 获取工种信息
export const getWorkerTypes = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/workerTypes/employees/dicts`
  return request.get(url, {
    params: params
  })
}
// 获取主维修工位
export const getRepairPositions = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/repairpositions/employee/dicts`
  return request.get(url, {
    params: params
  })
}
// 获取部门
export const getOrgsTree = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgs/getIsValid/Orgs`
  return request.get(url, {
    params: params
  })
}
