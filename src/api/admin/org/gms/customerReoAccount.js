import request from '@/utils/request'
import SERVER from '../../../server'

// 获取页面信息
export const queryCustomerReoAccount = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_MANAGE}/basedata/customerReoAccount`
  return request.get(url, {
    params: params
  })
}
// 根据id获取页面信息
export const getCustomerReoAccount = (id) => {
  const url = `${SERVER.GMSCLOUD_MANAGE}/basedata/customerReoAccount/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
// 新增保存
export const addCustomerReoAccount = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_MANAGE}/basedata/customerReoAccount/addCustomerReoAccount`
  return request.post(url, params)
}
// 编辑保存
export const editCustomerReoAccount = (params = {}, id) => {
  const url = `${SERVER.GMSCLOUD_MANAGE}/basedata/customerReoAccount/${id}/editCustomerReoAccount`
  return request.put(url, params)
}
// 往来客户选择
export const searchCustomerReo = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_MANAGE}/basedata/customerReoAccount/searchCustomerReo`
  return request.get(url, {
    params: params
  })
}
// 启用禁用
export const enableOrDisableCustomerReoAccount = (id, type) => {
  const url = `${SERVER.GMSCLOUD_MANAGE}/basedata/customerReoAccount/${id}/${type}`
  return request.post(url)
}
