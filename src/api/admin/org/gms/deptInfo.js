import http from '@/utils/request'
import SERVER from './../../../server'

// 查询部门定义信息
export function getDept(id) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgs/${id}`
  return http.get(url)
}
// 加载树
export function getTree(urlTree) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}${urlTree}`
  return http.get(url)
}
// 上级组织
export function getSuper() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgs/getParents/super`
  return http.get(url)
}
// 新增
export const addDept = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgs`
  return http.post(url, params)
}
// 新增
export const updateDept = (ids, params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgs/${ids}`
  return http.put(url, params)
}
