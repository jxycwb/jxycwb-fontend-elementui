import request from '@/utils/request'
import SERVER from '../../../server'

// 查询服务人员等级列表
export const getServiceLevel = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/serviceLevel`
  return request.get(url, {
    params: params
  })
}
// 批量修改服务等级
export const editServiceLevel = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/serviceLevel/editSave`
  return request.get(url, { params: params })
}
