import request from '@/utils/request'
import SERVER from './../../../server'

// 获取山推用户信息
export const getUsersList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms`
  return request.get(url, {
    params: params
  })
}
// 获取管理员用户信息
export const getAdminUserList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/admin`
  return request.get(url, {
    params: params
  })
}
// 获取代理商用户信息
export const getDealerUsersList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/dealerEmp`
  return request.get(url, {
    params: params
  })
}
// 获取代理商用户信息
export const getDealerUsersList2 = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/dealerEmp2`
  return request.get(url, {
    params: params
  })
}
// 新增用户信息
export const addUserInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms`
  return request.post(url, params)
}
// 根据id获取用户信息
export const getUserInfoById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/${id}`
  return request.get(url, {
    id: id
  })
}

export const findTcCodeByType = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/findTcCodeByType/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
export const findEmployeeRoleById = (id) => {
  const url = `${SERVER.GMSCLOUD_BASEDATA}/basedata/employees/findEmployeeRoleById/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
// 编辑用户信息
export const editUserInfo = (params = {}, id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/${id}`
  return request.put(url, params)
}
// 编辑管理员信息
export const editAdminUserInfo = (params = {}, id) => {
  debugger
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/${id}/admin`
  return request.post(url, params)
}
// 获取职务信息
export const getPositionType = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_BASEDATA}/basedata/positions/duty/dicts`
  return request.get(url, {
    params: params
  })
}
// 获取班组信息
export const getWorkGroup = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_REPAIR}/basedata/workgroups/dicts/select`
  return request.get(url, {
    params: params
  })
}
// 获取工种信息
export const getWorkerTypes = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_REPAIR}/basedata/workerTypes/employees/dicts`
  return request.get(url, {
    params: params
  })
}
// 获取主维修工位
export const getRepairPositions = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_REPAIR}/basedata/repairpositions/employee/dicts`
  return request.get(url, {
    params: params
  })
}
// 获取部门
export const getOrgsTree = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_BASEDATA}/basedata/orgs/getIsValid/Orgs`
  return request.get(url, {
    params: params
  })
}
// 获取经销商信息
export const getDealerSelectList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/dealer`
  return request.get(url, {
    params: params
  })
}
// 获取大区信息
export const getRegionSelectList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/region`
  return request.get(url, {
    params: params
  })
}
// 获取二级经销商信息
export const getChildDealerSelectList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/childDealer`
  return request.get(url, {
    params: params
  })
}
// 获取经销商table信息
export const getDealerTableList = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/dealer/${id}`
  return request.get(url)
}

// 根据id获取用户信息
export const getUserRoles = (dealerCode) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/${dealerCode}/roles`
  return request.get(url)
}

// 用户信息导出
export const getUsersListForExport = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/getUsersListForExport`
  return request.get(url, {
    params: params
  })
}

// 代理商用户信息导出
export const exportDeaEmpForSearch = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/exportDeaEmpForSearch`
  return request.get(url, {
    params: params
  })
}
// 代理商用户信息导出（）
export const exportDeaEmpForSearch2 = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/exportDeaEmpForSearch2`
  return request.get(url, {
    params: params
  })
}
