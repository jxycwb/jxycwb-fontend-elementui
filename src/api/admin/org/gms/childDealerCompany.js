import request from '@/utils/request'
import SERVER from '../../../server'

// 获取页面信息
export const queryDealerCompanyList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/childDealerChannel`
  return request.get(url, {
    params: params
  })
}
// 获取编辑页面信息
export const queryChildDealerComById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/childDealerChannel/${id}`
  return request.get(url)
}
// 获取厂端二级代理编辑页面信息
export const queryGmsChildDealerComById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/childDealerChannel/gms/${id}`
  return request.get(url)
}
// 编辑保存二级营业网点
export const editChildDealerChannel = (id, params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/childDealerChannel/editSave/${id}`
  return request.post(url, params)
}
// 新增保存二级营业网点
export const addDealerChannel = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/childDealerChannel/addSave`
  return request.post(url, params)
}
// 导出
export const exportDealerCompanyList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/childDealerChannel/export`
  return request.get(url, {
    params: params
  })
}
