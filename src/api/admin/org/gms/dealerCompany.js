import request from '@/utils/request'
import SERVER from '../../../server'

// 获取页面信息
export const queryListDealerCompany = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel`
  return request.get(url, {
    params: params
  })
}
// 按照ID查询经销商公司
export const getDealerCompanyById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/getDealerChannelByID/${id}`
  return request.get(url)
}

// 编辑经销商公司
export const editDealerChannel = (id, params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/editDealerChannel/${id}`
  return request.post(url, params)
}

export const getStructure = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/getStructure`
  return request.post(url, params)
}

// 按照ID查询经销商授权
export const getDealerArea = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/getDealerArea/${id}`
  return request.get(url)
}

// 新增/编辑经销商公司
export const addDealerChannel = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/addDealerChannel`
  return request.post(url, params)
}

// 提报经销商公司信息
export const commitDealerChannel = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/commitDealerChannel`
  return request.post(url, params)
}
//
export const queryDealerInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerGroup`
  return request.get(url, {
    params: params
  })
}
// 导出
export const exportDealerInfos = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/export/excel`
  return request.get(url, {
    params: params
  })
}

export const getAddress = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/getAddress`
  return request.get(url, {
    params: params
  })
}

export const getAddressById = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/getAddressById`
  return request.get(url, {
    params: params
  })
}

export const delAddress = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/delAddress`
  return request.get(url, {
    params: params
  })
}

export const saveAddress = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/saveAddress`
  return request.post(url, params)
}

export const getEquity = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/getEquity`
  return request.get(url, {
    params: params
  })
}

export const getEquityById = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/getEquityById`
  return request.get(url, {
    params: params
  })
}

export const delEquity = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/delEquity`
  return request.get(url, {
    params: params
  })
}

export const saveEquity = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/saveEquity`
  return request.post(url, params)
}

export const agreeOrReject = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/agreeOrReject`
  return request.post(url, params)
}

export const recommit = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/recommit`
  return request.post(url, params)
}

// 车型车系产品组查询
export function getTmModelParamByType(paramType) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/modelconfig/param/getTmModelParamByType/${paramType}`
  return request.get(url)
}

// 代理商维护导出
export const exportDealerDateInfos = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/exportDealerDateInfos/excel`
  return request.get(url, {
    params: params
  })
}

// 经销商信息选择
export const getDealerInfoSelect = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerChannel/getDealerInfoSelect`
  return request.get(url, {
    params: params
  })
}
