import request from '@/utils/request'
import SERVER from './../../../server'

// 查询职位列表
export const getPositionDefineList = (params = {}) => {
  const url = `${SERVER.GMSCLOUD_MANAGE}/basedata/positions`
  return request.get(url, {
    params: params
  })
}
// 编辑保存
export const editSavePositionDefine = (id, params = {}) => {
  const url = `${SERVER.GMSCLOUD_MANAGE}/basedata/positions/${id}`
  debugger
  return request.put(url, params)
}
