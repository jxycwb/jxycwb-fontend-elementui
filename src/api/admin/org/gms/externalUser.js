import request from '@/utils/request'
import SERVER from './../../../server'

// 获取外部用户信息
export const getUsersList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/externalUser`
  return request.get(url, {
    params: params
  })
}
// 禁用、启用
export const disbaledInfoById = (id, isDisable) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/externalUser/${id}/${isDisable}`
  return request.put(url, {
    id: id,
    isDisable: isDisable
  })
}
// 查询导出数据(前端实现)
export const queryForExport = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/externalUser/queryForExport`
  return request.get(url, {
    params: params
  })
}
