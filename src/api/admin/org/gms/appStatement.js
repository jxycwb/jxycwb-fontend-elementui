import request from '@/utils/request'
import SERVER from './../../../server'

// 车辆绑定信息报表列表
export const getVehicleBindingList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/appStatement/vehicleBinding`
  return request.get(url, {
    params: params
  })
}

// 车辆绑定信息报表列表导出(前端实现)
export const queryVehicleBindingExport = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/appStatement/vehicleBinding/queryForExport`
  return request.get(url, {
    params: params
  })
}
