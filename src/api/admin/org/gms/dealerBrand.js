import request from '@/utils/request'
import SERVER from './../../../server'

// 查询品牌经销商基本关联信息
export const getDealerBrand = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/vehicleProduct/dealerBrand`
  return request.get(url, {
    params: params
  })
}

// 新增经销商基本信息
export const addrelation = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/vehicleProduct/dealerBrand/save`
  return request.post(url, params)
}

// 删除关联关系
export const deleteRelation = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/vehicleProduct/dealerBrand/${id}`
  return request.delete(url, id)
}

// 设置主品牌
export const primaryDealerBrand = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/vehicleProduct/dealerBrand/primaryBrand/${id}`
  return request.put(url, id)
}

export const relateDealerBrand = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/vehicleProduct/dealerBrand/relate`
  return request.put(url, params)
}
