import http from '@/utils/request'
import SERVER from '@/api/server'

export function selectDealer(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/dealer/selectVirtualDealer`
  return http.get(url, {
    params: params
  })
}
export function editDealer(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/dealer/editDealer`
  return http.post(url, params
  )
}
