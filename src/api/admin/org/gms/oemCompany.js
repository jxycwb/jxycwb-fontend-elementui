import request from '@/utils/request'
import SERVER from '../../../server'

// 查询组织列表
export const getOemComList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/car/factory`
  return request.get(url, {
    params: params
  })
}
// 保存
export const saveOemComList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/car/factory`
  return request.post(url, params)
}
// 编辑保存
export const editSaveOemComList = (id, params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/car/factory/${id}`
  debugger
  return request.put(url, params)
}
