import request from '@/utils/request'
import SERVER from '../../../server'

// 查询组织列表
export const getComOrgList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org`
  return request.get(url, {
    params: params
  })
}
// 查询组织树列表
export const getComOrgTreeList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org/tree`
  return request.get(url, {
    params: params
  })
}
// 保存
export const saveComOrgList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org`
  return request.post(url, params)
}
// 编辑保存
export const editSaveComOrgList = (id, params = {}) => {
  debugger
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org/${id}`
  return request.post(url, params)
}

// 查询组织列表
export const getParentOrgList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org/parentOrg`
  return request.get(url, {
    params: params
  })
}

// 查询组织下面的经销商列表
export const getOrgDealerList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org/orgDealer`
  return request.get(url, {
    params: params
  })
}

// 查询组织列表
export const getOrgInfoTree = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org/getOrgInfoTree`
  return request.get(url, {
    params: params
  })
}
