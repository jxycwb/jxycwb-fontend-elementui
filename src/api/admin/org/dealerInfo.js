import http from '@/utils/request'
import SERVER from './../../server'

// 查询经销商基本信息
export function getDealer() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealer`
  return http.get(url)
}

// 编辑修改经销商基本信息
export const updateDealer = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealer`
  return http.put(url, params)
}

// 查询经销商基本信息列表
export function getDealerGroup() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerGroup/dealer`
  return http.get(url)
}
// 查询所有经销商
export const getAllDealerInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealerInfos`
  return http.get(url, {
    params: params
  })
}
// 查询经销商渠道基本信息
export function getDealerCompanyByCode() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dealer/getDealerChannelByCode`
  return http.get(url)
}
