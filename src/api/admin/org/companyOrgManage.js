import request from '@/utils/request'
import SERVER from '../../server'

// 查询组织列表
export const getComOrgList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org`
  return request.get(url, {
    params: params
  })
}
// 查询组织树列表
export const getComOrgTreeList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org/tree`
  return request.get(url, {
    params: params
  })
}
// 保存
export const saveComOrgList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org`
  return request.post(url, params)
}
// 编辑保存
export const editSaveComOrgList = (id, params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/company/org/${id}`
  debugger
  return request.put(url, params)
}

