import request from '@/utils/request'
import SERVER from '../../../server'

export const queryCusAndCarsInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/imtool/cusAndCarsInfo`
  return request.post(url, params)
}
// 查询销售顾问客户的统计信息·
export const querySalesAdvisorCustomCount = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/report/querySalesAdvisorCustomCount`
  return request.post(url, params)
}
// 查询销售顾问客户的明细信息·
export const querySalesAdvisorCustomDetail = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/report/querySalesAdvisorCustomDetail`
  return request.post(url, params)
}
// 查询服务顾问客户的统计信息·
export const queryServicesAdvisorCustomCount = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/report/queryServicesAdvisorCustomCount`
  return request.post(url, params)
}
// 查询服务顾问客户的明细信息·
export const queryServicesAdvisorCustomDetail = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/report/queryServicesAdvisorCustomDetail`
  return request.post(url, params)
}
