import request from '@/utils/request'
import SERVER from './../../../server'

// 获取角色列表
export const getOperateLogList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/operateLog`
  return request.get(url, {
    params: params
  })
}
