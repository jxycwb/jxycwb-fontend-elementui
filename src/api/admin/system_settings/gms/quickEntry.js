import request from '@/utils/request'
import SERVER from './../../../server'

// 获取角色列表
export const getQuickEntryList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/quickEntry`
  return request.get(url, {
    params: params
  })
}

export const quickEntryData = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/quickEntry/${id}`
  return request.get(url)
}

// 新增角色
export const addQuickEntry = (params = {}, data = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/quickEntry`
  return request({
    url: url,
    method: 'post',
    params: params,
    data: data
  })
}

// 更新角色
export const updateQuickEntry = (params = {}, data = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/quickEntry/update`
  return request({
    url: url,
    method: 'post',
    params: params,
    data: data
  })
}

// 调整排序
export const changeQuickEntrySort = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/quickEntry/move`
  return request.post(url, params)
}
