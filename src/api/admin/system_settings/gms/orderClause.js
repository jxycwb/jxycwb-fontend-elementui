import request from '@/utils/request'
import SERVER from '../../../server'

// 查询订购协议列表
export const getClauseList = (params = {}) => {
  const url = `/api/clause/list`
  return request.get(url, {
    params: params
  })
}
// 新增订购协议
export const addNewClause = (params = {}) => {
  const url = `/api/clause/addClause`
  return request.post(url, params)
}
// 编辑订购协议
export const editClause = (params = {}) => {
  const url = `/api/clause/editClause`
  return request.post(url, params)
}
// 根据id查订协议
export const selectById = (id) => {
  const url = `/api/clause/getById/${id}`
  return request.get(url, { id: id })
}
