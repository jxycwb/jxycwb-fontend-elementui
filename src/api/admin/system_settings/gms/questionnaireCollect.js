import request from '@/utils/request'
import SERVER from '../../../server'

// 查询车系定金列表
export const getCollect = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/questionnaireCollect`
  return request.get(url, {
    params: params
  })
}
// 根据id查问卷
export const selectById = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/questionnaireCollect/${id}`
  return request.get(url, { id: id })
}
export const selectQuestionById = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/questionnaireCollect/selectQuestionById/${id}`
  return request.get(url, { id: id })
}

