import request from '@/utils/request'
import SERVER from '../../../server'

// 查询车系定金列表
export const getEarnest = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/earnest`
  return request.get(url, {
    params: params
  })
}
// 新增车系定金
export const addNewEarnest = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}//earnest/addEarnest`
  return request.post(url, params)
}
// 编辑车系定金
export const editEarnest = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/earnest/editEarnest`
  return request.post(url, params)
}
// 根据id查车系定金
export const selectById = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/earnest/${id}`
  return request.get(url, { id: id })
}
// 查询车系
export const findSeries = () => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/earnest/findSeries`
  return request.get(url)
}
