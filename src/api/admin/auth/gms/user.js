import request from '@/utils/request'
import SERVER from '../../../server'

// 获取账户信息
export const searchUserInfos = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/userInfo`
  return request.get(url, {
    params: params
  })
}
// 修改密码
export const editPassword = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/userInfo/editPassword`
  return request.post(url, params)
}
// 重置密码
export const restPassword = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/userInfo/restPassword`
  return request.post(url, params)
}
