import request from '@/utils/request'
import SERVER from './../../../server'

/* {
    id:Number(id)
  })*/
// 获取功能树
export const getMenu = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/roleDataAdd/10461002`
  return request.get(url, {
    params: params
  })
}
export const getDealerMenu = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/roleDataAdd/10461001`
  return request.get(url, {
    params: params
  })
}
export const roleData = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/roleData/${id}`
  return request.get(url)
}

export const employeeData = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/employeeData/${id}`
  return request.get(url)
}

// 获取菜单信息操作权限
export const getRoleMenuRange = (menuId, roleIds) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/roleMenuRange/${menuId}/${roleIds}`
  return request.get(url)
}

// 获取菜单信息操作按钮
export const getRoleMenuAction = (menuId, roleIds) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/roleMenuAction/${menuId}/${roleIds}`
  return request.get(url)
}

// 获取角色列表
export const getRoleList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles`
  return request.get(url, {
    params: params
  })
}

// 新增角色
export const addRole = (params = {}) => {
  debugger
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles`
  return request.post(url, params)
}

// 更新角色
export const upRole = (params = {}) => {
  debugger
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/upd`
  return request.post(url, params)
}

// 分配角色权限
export const setRoleAuth = (id, params) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/permission/${id}`
  return request.put(url, params)
}

export const setAppRoleAuth = (id, params) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/permissionApp/${id}`
  return request.put(url, params)
}

// 更新角色
export const updateRole = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/permission`
  return request.post(url, params)
}

// 删除角色
export const deleteRole = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/${id}`
  return request.delete(url, {
    id: Number(id)
  })
}

// 个人参数配置
// 获取维修参数配置列表
export const getMaintain = (roleId) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/roleMaintain/${roleId}`
  return request.get(url)
}

// 获取配件参数配置列表
export const getAccessories = (roleId) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/roleAccessories/${roleId}`
  return request.get(url)
}

// 获取整车仓库参数配置列表
export const getVehicleWarehouse = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/slstore/vehicleWarehouse/checks`
  return request.get(url, params)
}

// 获取配件库参数配置列表
export const getPartAccessories = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/slstore/pjck/accessories/partAuth`
  return request.get(url, params)
}

// 获取配件库参数配置列表
export const getJpckAccessories = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/slstore/jpck/accessories/powerChecks`
  return request.get(url, params)
}

// 获取优惠模式库参数配置列表
export const getDiscountMode = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_REPAIR}/basedata/gms/discountmodes/discountMode/permissionDicts`
  return request.get(url, params)
}

// 获取功能树
export const getAppMenu = (params) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/gms/roles/appMenu`
  return request.get(url, {
    params: params
  })
}
