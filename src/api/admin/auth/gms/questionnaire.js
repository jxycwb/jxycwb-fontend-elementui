/*
 * @Author: wenjia-chen 1119024121@qq.com
 * @Date: 2022-10-20 11:39:37
 * @LastEditors: wenjia-chen 1119024121@qq.com
 * @LastEditTime: 2022-10-25 15:04:12
 * @FilePath: \dmscloud-web\src\api\admin\auth\gms\questionnaire.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/utils/request'
import SERVER from '../../../server'

// 页面查询
export const queryQuestionnaire = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/questionnaire`
  return request.get(url, {
    params: params
  })
}

// 新增问卷
export const addQuestionnaire = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/questionnaire`
  return request.post(url, params)
}

export const getInfoById = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/questionnaire/${id}`
  return request.get(url, {
    id: id
  })
}

export const editQuestionnaire = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/questionnaire/edit`
  return request.post(url, params)
}

// 发布
export const publish = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/questionnaire/publish`
  return request.post(url, params)
}
