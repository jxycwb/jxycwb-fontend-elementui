import request from '@/utils/request'
import SERVER from '../../../server'

// 查询授权管理列表
export const getAuthList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/authorization`
  return request.get(url, {
    params: params
  })
}
// 编辑列表
export const getAuthListById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/authorization/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
// 停用
export const stopById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/authorization/vue/${id}`
  return request.delete(url)
}
// 停用
export const startById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/authorization/start/${id}`
  return request.get(url)
}
// 弹出框职位模板列表
export const getModelList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition`
  return request.get(url, {
    params: params
  })
}
// 保存
export const SaveAuth = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/authorization`
  return request.post(url, params)
}
// 白名单
export const whiteEnable = (id, whiteStatus) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/authorization/whiteEnable/${id}/${whiteStatus}`
  return request.get(url)
}
