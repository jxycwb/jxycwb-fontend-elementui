import request from '@/utils/request'
import SERVER from '../../../server'

// 查询职位列表
export const getPositionAuthList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition`
  return request.get(url, {
    params: params
  })
}
// 编辑角色列表
export const getPositionAuthRoleList = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition/role/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
// 编辑组织列表
export const getPositionAuthOrgList = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition/depart/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
// 查询角色模板列表
export const getPositionAuthRoleModelList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles`
  return request.get(url, {
    params: params
  })
}
// 查询角色组织树
export const getPositionAuthRoleTree = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgs/getIsValid/Orgs`
  return request.get(url, {
    params: params
  })
}

// 保存
export const groupSavePositionAuth = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition`
  return request.post(url, params)
}
// 编辑保存
export const editSavePositionAuth = (id, params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition/${id}`
  debugger
  return request.put(url, params)
}

// 删除
export const deleteInfoById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition/${id}/delete`
  return request.put(url)
}
