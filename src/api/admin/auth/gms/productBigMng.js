import request from '@/utils/request'
import SERVER from '../../../server'

// 查询产品大类信息
export const queryList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_SERVICE}/basedata/productBigMng/queryList`
  return request.get(url, { params: params })
}

// 根据ID查询产品大类信息
export const queryProductBigMngById = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_SERVICE}/basedata/productBigMng/queryProductBigMngById`
  return request.get(url, { params: params })
}

// 保存产品信息
export const saveInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_SERVICE}/basedata/productBigMng/saveInfo`
  return request.get(url, { params: params })
}

// 查询产品系数大类信息
export const queryParamList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/productParam/queryParamList`
  return request.get(url, { params: params })
}

// 保存产品大类系数信息
export const saveParamInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/productParam/saveParamInfo`
  return request.get(url, { params: params })
}
