import request from '@/utils/request'
import SERVER from '../../../server'

// 查询试驾信息
export const queryDrive = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/userDrive/queryDrive`
  return request.get(url, {
    params: params
  })
}

// 导出活动信息
export const exportDrive = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/userDrive/exportDrive`
  return request.get(url, {
    params: params
  })
}

// 获取经销商信息
export const getDealer = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/userDrive/getDealer`
  return request.get(url, {
    params: params
  })
}

// 获取经销商信息
export const getSell = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/userDrive/getSell`
  return request.get(url, {
    params: params
  })
}

// 编辑试驾的信息
export const editDrive = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/userDrive/editDrive`
  return request.get(url, {
    params: params
  })
}
