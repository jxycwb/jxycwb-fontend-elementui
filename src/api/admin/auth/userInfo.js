import request from '@/utils/request'
import SERVER from '../../server'

// 查询职位列表
export function getUserInfoList() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/users/loginuser`
  return request.get(url)
}
// 查询部门
export function getDeptList() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgs/getIsValid/Orgs`
  return request.get(url)
}
export const editConfirm = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/users/loginuser`
  return request.put(url, params)
}
// 职务
export function getPositionList() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/positions/duty/dicts`
  return request.get(url)
}
// 工种
export function getworkerTypeList() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/workerTypes/employees/dicts`
  return request.get(url)
}
// 主维修工位
export function getDefaultPositionList() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/repairpositions/employee/dicts`
  return request.get(url)
}
// tc_code checkebox
export const findTcCodeByType = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/findTcCodeByType/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
export const supereditpassword = (params = {}) => {
  const url = `/api/login/editpasswordByOldPassword`
  return request.post(url, params)
}

// 发送短信验证码（修改密码使用）
export function SendSmsForPasswordChange(phone) {
  const params = {
    phone: phone
  }
  const url = `${SERVER.DMSCLOUD_BASEDATA}/smssend/SendSmsForPasswordChange/interAspect`
  return request.get(url, { params })
}

// 发送短信验证码（忘记密码使用）
export function sendForgetPwdMessage(phone) {
  const params = {
    phone: phone
  }
  const url = `${SERVER.DMSCLOUD_BASEDATA}/smssend/sendForgetPwdMessage/interAspect`
  return request.get(url, { params })
}

// 校验用户输入的验证码正确性
export function CheckSmsVertifyCode(phone, inputVcode) {
  const params = {
    phone: phone,
    inputVcode: inputVcode
  }
  const url = `${SERVER.DMSCLOUD_BASEDATA}/smssend/CheckSmsVertifyCode/interAspect`
  return request.get(url, { params })
}

// 校验用户账号-手机正确性
export const validateUserInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/userInfo/validateUserInfo/interAspect`
  return request.get(url, { params: params })
}

// 修改密码（忘记密码）
export const changePassword = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/userInfo/changePassword/interAspect`
  return request.put(url, params)
}

// 查询经销商
export function getDealerInfo(params = {}) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/users/getDealerInfo`
  return request.get(url, { params })
}

// 查询经销商
export function getDealerInfoYJY(params = {}) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/users/getDealerInfoYJY`
  return request.get(url, { params })
}
