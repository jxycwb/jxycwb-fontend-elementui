import request from '@/utils/request'
import SERVER from '../../../server'

export const sdkConfig = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/sdkOauth/sdkConfig`
  return request.post(url, params)
}

export const sdkAgentConfig = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/sdkOauth/agentConfig`
  return request.post(url, params)
}

export const oauthLogin = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/sdkOauth/login`
  return request.get(url, {
    params: params
  })
}

export const doLogin = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/wechat/sdkOauth/doLogin`
  return request.get(url, {
    params: params
  })
}
