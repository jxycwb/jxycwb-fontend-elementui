import SERVER from './../../server'
import request from '@/utils/request'
// 查询员工信息(员工权限编辑页面)
export const searchRoleEmp = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/permission/items`
  return request.get(url, {
    params: params
  })
}
// 管理员解锁账户
export const removeLockflag = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/removelock/${id}`
  return request.post(url, {
    id: id
  })
}
// 管理员锁定账户
export const lockflag = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/gms/lock/${id}`
  return request.post(url, {
    id: id
  })
}
// 根据id查找员工信息
export const findByEmployeeId = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/employees/${id}`
  return request.get(url, {
    id: id
  })
}
// 用户对应的权限的新增及修改
export const ModifyOrg = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/users/permission`
  return request.put(url, params)
}
// 加载用户权限信息
export const userData = (ids) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/users/userData`
  return request.get(url, ids)
}
export const userDataVue = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/users/userDataVue/${id}`
  return request.get(url, id)
}
// 查询职位菜单操作按钮 复选框组
export const actionRemoteUrl = (menuId, positionIds) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition/positionMenuAction/${menuId}/${positionIds}`
  return request.get(url, Number(menuId), Number(positionIds))
}
// 查询角色菜单数据范围权限  复选框组
export const rangeRemoteUrl2 = (menuId, positionIds) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition/positionMenuRange/${menuId}/${positionIds}`
  return request.get(url)
}
// 获取所有职务信息
export const getorgPositionType = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition/type`
  return request.get(url, {
    params: params
  })
}
// 管理员修改密码
export const supereditpassword = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/users/supereditpassword`
  return request.put(url, params
  )
}
// 职位保存
export const roleData = (roleIds) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/orgPosition/positionData/${roleIds}`
  return request.get(url, {
    id: roleIds
  })
}
