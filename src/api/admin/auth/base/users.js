import request from '@/utils/request'
import SERVER from './../../../server'

// 获取用户列表
export const getUsersList = (params = {}) => {
  const url = `${SERVER.USERSERVER}/user/getAllUsermap`
  return request.post(url, params)
}

// 新增用户
export const createNewUser = (params = {}) => {
  const url = `${SERVER.USERSERVER}/user/create`
  return request.post(url, params)
}

// 启用
export const normal = (id) => {
  const url = `${SERVER.USERSERVER}/user/normal/${id}`
  return request.post(url, {
    id: Number(id)
  })
}

// 停用
export const stop = (id) => {
  const url = `${SERVER.USERSERVER}/user/stop/${id}`
  return request.post(url, {
    id: Number(id)
  })
}

// 编辑修改用户
export const updateUsers = (params = {}) => {
  const url = `${SERVER.USERSERVER}/user/updateUsers`
  return request.post(url, params)
}

// 密码重置
export const reset = (id) => {
  const url = `${SERVER.USERSERVER}/user/reset/${id}`
  return request.get(url, { id: Number(id) })
}

/* {
    id:Number(id)
  })*/

// 获取功能树
export const getMenu = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/roleData`
  return request.post(url, params)
}

// 添加功能节点
export const addTreeNode = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/addFunction`
  return request.post(url, params)
}

// 移除功能节点
export const removeTreeNode = (id) => {
  const url = `${SERVER.USERSERVER}/baseData/deleteFunction`
  const params = {
    id: id,
    isdelpre: true
  }
  return request.post(url, params)
}

// 更新功能节点
export const updateTreeNode = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/updateFunction`
  return request.post(url, params)
}

// 获取角色列表
export const getRoleList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles`
  return request.post(url, params)
}

// 添加角色类型
export const addRoleType = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/addRoleType`
  return request.post(url, params)
}

// 更新角色类型
export const updateRoleType = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/updateRoleType`
  return request.post(url, params)
}

// 删除角色类型
export const deleteRoleType = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/deleteRoleType`
  return request.post(url, params)
}

// 分配角色权限
export const setRoleAuth = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/assignPermiss`
  return request.post(url, params)
}

// 新增角色
export const addRole = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/addRole`
  return request.post(url, params)
}

// 更新角色
export const updateRole = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/updateRole`
  return request.post(url, params)
}

// 获取密钥列表
export const querySecretKeyList = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/getChannel`
  return request.post(url, params)
}
// 新增密钥
export const addSecretKey = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/addChannel`
  return request.post(url, params)
}
// 更新密钥
export const updateSecretKey = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/updateChannel`
  return request.post(url, params)
}
// 删除密钥
export const deleteSecretKey = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/deleteChannel`
  return request.post(url, params)
}
// 生成密钥
export const genSecretKey = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/genSecretKey`
  return request.post(url, params)
}

// 获取组织列表
export const queryOrgList = (params = {}) => {
  const url = `${SERVER.USERSERVER}/init/groupList`
  return request.post(url, params)
}

// 服务类型
export const queryServeType = (params = {}) => {
  const url = `${SERVER.USERSERVER}/init/serveTypeList`
  return request.post(url, params)
}

// 角色类型列表（下拉选初始化）
export const queryRoleTypeList = (params = {}) => {
  const url = `${SERVER.USERSERVER}/init/roleTypeList`
  return request.post(url, params)
}

// 初始化数据状态
export const initStatus = (params = {}) => {
  const url = `${SERVER.USERSERVER}/init/initStatus`
  return request.post(url, params)
}

// 组织级别初始化数据
export const queryOrgLevelList = (params = {}) => {
  const url = `${SERVER.USERSERVER}/init/orgLevelList`
  return request.post(url, params)
}

// 组织类型列表
export const getOrgTypeList = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/getOrgType`
  return request.post(url, params)
}

// 添加组织类型
export const addOrgType = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/addOrgType`
  return request.post(url, params)
}

// 更新组织类型
export const updateOrgType = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/updateOrgType`
  return request.post(url, params)
}

// 删除组织类型
export const deleteOrgType = (params = {}) => {
  const url = `${SERVER.USERSERVER}/baseData/deleteOrgType`
  return request.post(url, params)
}
