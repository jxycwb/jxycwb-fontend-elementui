import request from '@/utils/request'
import SERVER from './../../server'

/* {
    id:Number(id)
  })*/
// 获取功能树
export const getMenu = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/roleData`
  return request.get(url, {
    params: params
  })
}
export const roleData = (id, dataType) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/roleData/${id}`
  return request.get(url, {
    params: { dataType: dataType }
  })
}

// 获取菜单信息操作权限
export const getRoleMenuRange = (menuId, roleIds) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/roleMenuRange/${menuId}/${roleIds}`
  return request.get(url)
}

// 获取菜单信息操作按钮
export const getRoleMenuAction = (menuId, roleIds) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/roleMenuAction/${menuId}/${roleIds}`
  return request.get(url)
}

// 获取角色列表
export const getRoleList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles`
  return request.get(url, {
    params: params
  })
}

// 新增角色
export const addRole = (params = {}) => {
  debugger
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles`
  return request.post(url, params)
}

// 分配角色权限
export const setRoleAuth = (id, params) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/permission/${id}`
  return request.put(url, params)
}

// 更新角色
export const updateRole = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/permission`
  return request.post(url, params)
}

// 删除角色
export const deleteRole = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/${id}`
  return request.delete(url, {
    id: Number(id)
  })
}

// 个人参数配置
// 获取维修参数配置列表
export const getMaintain = (roleId) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/roleMaintain/${roleId}`
  return request.get(url)
}

// 获取配件参数配置列表
export const getAccessories = (roleId) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/roles/roleAccessories/${roleId}`
  return request.get(url)
}

// 获取整车仓库参数配置列表
export const getVehicleWarehouse = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/vehicleStock/vehicleWarehouse/checks`
  return request.get(url, params)
}

// 获取配件库参数配置列表
export const getPartAccessories = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/store/pjck/accessories/powerChecks`
  return request.get(url, params)
}

// 获取配件库参数配置列表
export const getJpckAccessories = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/store/jpck/accessories/powerChecks`
  return request.get(url, params)
}

// 获取优惠模式库参数配置列表
export const getDiscountMode = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/discountmodes/discountMode/permissionDicts`
  return request.get(url, params)
}
