import request from '@/utils/request'
import SERVER from '../../server'

// 获取页面配置信息
export const getPageInfo = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/page`
  return request.get(url, {
    params: params
  })
}
// 根据id获取页面配置信息
export const getPageInfoById = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/page/${id}`
  return request.get(url, {
    id: Number(id)
  })
}
export const getPageRequired = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/page/${id}/pageFiled/required`
  return request.get(url, {
    id: Number(id)
  })
}
export const getPageDisplay = (id) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/page/${id}/pageFiled/display`
  return request.get(url, {
    id: Number(id)
  })
}
export const savePageFieldPermission = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/basedata/page/savePageFieldPermission`
  return request.post(url, params)
}
