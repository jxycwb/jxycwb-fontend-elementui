/*
 * @Author: wenjia-chen 1119024121@qq.com
 * @Date: 2022-11-16 09:21:10
 * @LastEditors: wenjia-chen 1119024121@qq.com
 * @LastEditTime: 2022-11-24 15:45:09
 * @FilePath: \jmc_shop_pc\src\api\dashboard.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import http from '@/utils/request'
import SERVER from './server'

// 首页
export function getCardList() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dashboard/getCardList`
  return http.get(url)
}

export const getEchartsLineData = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dashboard/getEchartsLineData`
  return http.get(url, {
    params: params
  })
}

export const exportMessage = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dashboard/exportMessage`
  return http.get(url, {
    params: params
  })
}

export const getEchartsBarData = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dashboard/getEchartsBarData`
  return http.get(url, {
    params: params
  })
}

export const getModelsDataBySeries = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dashboard/getModelsDataBySeries`
  return http.get(url, {
    params: params
  })
}

