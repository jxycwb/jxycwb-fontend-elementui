import request from '@/utils/request'
import SERVER from '@/api/server'

export const queryMsg = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/actMsgMaintenance/queryMsg`
  return request.get(url, {
    params: params
  })
}

export const exportMsg = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/actMsgMaintenance/exportMsg`
  return request.get(url, {
    params: params
  })
}

// 根据id将活动评论信息改为精彩评论
export const publishComments = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/actMsgMaintenance/publish/${id}`
  return request.post(url)
}

//  根据id将针对精彩评论进行撤销
export const cancelComments = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/actMsgMaintenance/cancel/${id}`
  return request.post(url)
}

export const respond = (params = {}, id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/actMsgMaintenance/respond/${id}`
  return request.get(url, {
    params: params
  })
}

