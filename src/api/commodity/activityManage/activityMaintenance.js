import request from '@/utils/request'
// import SERVER from '../../../server'
import SERVER from '@/api/server'

// 获取活动列表
export const queryActivity = (params = {}) => {
  const url = `/api/activity/page`
  return request.get(url, {
    params: params
  })
}

// 添加活动
export const addActivity = (params = {}) => {
  const url = `/api/activity/save`
  return request.post(url, params)
}

// 编辑活动信息
export const editActivity = (params = {}, id) => {
  const url = `/api/activity/update/${id}`
  return request.post(url, params)
}

export const effective = (params = {}, id) => {
    const url = `/api/activity/effective/${id}`
    return request.post(url, params)
  }

// 预约发布时间
export const setReleaseTime = (params, id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/activityMaintenance/release/${id}`
  return request.post(url, params)
}

// 导出活动信息
export const exportActivity = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/activityMaintenance/exportActivity`
  return request.get(url, {
    params: params
  })
}

export const editStatus = () => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/activityMaintenance/editStatus`
  return request.get(url)
}

export const getDetail = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/activityMaintenance/getDetail4web/${id}`
  return request.get(url)
}

