import request from '@/utils/request'
// import SERVER from '../../../server'
import SERVER from '@/api/server'

// 获取活动列表
export const queryOrder = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/orderMaintenance/queryOrder`
  return request.get(url, {
    params: params
  })
}

// 导出订单信息
export const exportOrder = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/orderMaintenance/exportOrder`
  return request.get(url, {
    params: params
  })
}

// 获取订单状态信息
export const getOrderStatus = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/orderMaintenance/getOrderStatus/${id}`
  return request.get(url)
}

// 获取物流信息
export const getOrderLogistics = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/orderMaintenance/orderLogistics`
  return request.get(url, {
    params: params
  })
}

// 获取经销商信息
export const getDealer = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/orderMaintenance/getDealer`
  return request.get(url, {
    params: params
  })
}

// 将订单状态改为确认取消
export const confirmCancel = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/orderMaintenance/confirmCancel`
  return request.get(url, {
    params: params
  })
}

// 下发
export const down = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/vehicle/orderMaintenance/down`
  return request.get(url, {
    params: params
  })
}

