import http from '@/utils/request'
import SERVER from '@/api/server'

// 上传/导入
export function uploadFile(param) {
  return http({
    url: `/api/file/upload`,
    method: 'post',
    // params: param
    data: param,
    headers: { 'Content-Type': 'multipart/form-data;charset=utf-8' }
  })
}
