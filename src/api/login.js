import http from '@/utils/request'
import SERVER from './server'

export function login(params) {
  const url = `/api/login`
  return http.post(url, params)
}

export function dealerLogin(params) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/login/dealerLogin`
  return http.post(url, params)
}

export function initUserData(params) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/login/initUserData`
  return http.get(url, {
    params
  })
}

export function getInfo(token) {
  const url = `/api/login/getMenus`
  return http.get(url)
}

export function getDict(token) {
  const url = `/api/dicts`
  return http.get(url)
}

export function refreshDict() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/dicts/refresh`
  return http.get(url)
}

export function getAuthHandle(token) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/login/handles`
  return http.get(url)
}

export function logout() {
  const url = `/api/logout`
  return http.get(url)
}

export function foo() {
  const url = `/user/success`
  return http.get(url)
}
export function getData() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/commonDatas`
  return http.get(url)
}

// 收藏
export function favorite(menuId, isfvtid) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/login/favorite`
  return http.post(url, { menuId: menuId, isFavorite: isfvtid })
}

// 首页
export function getHomeData() {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/homePage/queryForSalesHome`
  return http.get(url)
}

// 首页
export function getPimp(params) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/login/pimp`
  return http.get(url, {
    params: params
  })
}

export function getCodeImg() {
  const url = `/api/login/code`
  return http.get(url)
}

export function callStart(param) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/callRecord/callStart`
  return http.post(url, param)
}

export function getFileList(params) {
  const url = `${SERVER.DMSCLOUD_BASEDATA}/commonDatas/getFileList`
  return http.get(url, {
    params: params
  })
}
