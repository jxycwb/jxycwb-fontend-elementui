const config = {
  USERSERVER: `/userServer`,
  ORDERERVER: `/api`,
  VEHICLESERVER: `/vehicle`,
  PAYCONSOLE: `/payConsole`,
  FINANCESERVER: `/finance`,
  YONYOUPAY: `/yonYouPay`,
  EINVOICE: `/einvoiceConsole`,
  INVOICESERVER: `/invoice_api`,
  POSSERVER: `/posServer`,

  CALLSERVER: `/zhongqi_web`,

  ISSERVER: `/IS-server`,

  FESERVER: `/fe`,

  DMSCLOUD_BASEDATA: `/dmscloud.basedata`,
  DMSCLOUD_PART: `/dmscloud.part`,
  DMSCLOUD_SERVICE: `/dmscloud.service`,
  // DMSCLOUD_WORKFLOW: '/dmscloud.workflow',
  DMSCLOUD_VEHICLE: `/dmscloud.vehicle`,
  DMSCLOUD_DLR: `/dmscloud.dlr`,
  DMSCLOUD_INTERFACEPLATFORM: `/dmscloud.interfacePlatform`,
  DMSCLOUD_VEHICLE_DEALER: `/dmscloud.ve.dealer`,
  // GMSCLOUD_BASEDATA: '/dcscloud.basedata',
  // GMSCLOUD_MANAGE: '/dcscloud.basedata',
  // DMSCLOUD_PART: '/vue.part',
  // GMSCLOUD_PART: '/dcscloud.part',
  // DMSCLOUD_REPAIR: '/vue.repair',
  // GMSCLOUD_REPAIR: '/dcscloud.repair',
  // DMSCLOUD_CUSTOMER: `/vue.customer`,
  // GMSCLOUD_CUSTOMER: `/dcscloud.customer`,
  // DMSCLOUD_FINANCE: `/vue.finance`,
  // DMSCLOUD_RETAIL: '/vue.retail',
  // GMSCLOUD_RETAIL: '/dcscloud.retail',
  // DMSCLOUD_VEHICLE: `/vue.vehicle`,
  // DMSCLOUD_POTENCUS: `/vue.potenCus`,
  // DMSCLOUD_MARKET: `/vue.market`,
  // DMSCLOUD_INVOICE: `/vue.invoice`,
  // DMSCLOUD_VALUEADDED: `/vue.valueAdded`,
  // DCSCLOUD_SALES: `/dcscloud.sales`,
  // DCSCLOUD_NEWENERGY: `/dcscloud.newEnergy`,
  // DMSCLOUD_USEDVEHICLE: `/vue.usedVehicle`,
  // DMSCLOUD_REPORT: `/vue.report`,
  // GMSCLOUD_REPORT: `/dcscloud.report`,
  // DMSCLOUD_TECHQUAL: `/vue.techQual`
  // CALL_URL: `https://sipcc1.shantui.com`
  // CALL_URL: 'https://400.cnhtcerp.com',
  DMSCLOUD_FINANCEABUTMENT: `/dmscloud.financeAbutment`

}
export default config
