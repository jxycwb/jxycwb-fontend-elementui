/*
 * @Author: wenjia-chen 1119024121@qq.com
 * @Date: 2022-10-27 10:10:06
 * @LastEditors: wenjia-chen 1119024121@qq.com
 * @LastEditTime: 2022-11-04 15:08:47
 * @FilePath: \jmc_shop_pc\src\api\vehicle\custBiz.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import http from '@/utils/request'
import SERVER from '@/api/server'

export function queryPageInfos4custBiz(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/queryPageInfos`
  return http.get(url, {
    params: params
  })
}

export function issued4custBiz(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/issued`
  return http.post(url, params)
}

export function queryPageInfos4browse(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/queryPageInfos4browse`
  return http.get(url, {
    params: params
  })
}

export function queryPageInfos4share(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/queryPageInfos4share`
  return http.get(url, {
    params: params
  })
}

export function queryPageInfos4try(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/queryPageInfos4try`
  return http.get(url, {
    params: params
  })
}

export function queryInfos4custBiz(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/queryInfos`
  return http.get(url, {
    params: params
  })
}

export function editUserName(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/editUserName`
  return http.get(url, {
    params: params
  })
}

// 查询车系
export const findSeries = () => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/earnest/findSeries`
  return http.get(url)
}

export function queryPage4OutClue(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/queryPage4OutClue`
  return http.get(url, {
    params: params
  })
}

export function queryOutClue4Export(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/queryOutClue4Export`
  return http.get(url, {
    params: params
  })
}

export function editOutClue(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/editOutClue`
  return http.post(url, params)
}

export function sendSmg(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/custBiz/sendSmg`
  return http.post(url, params)
}

