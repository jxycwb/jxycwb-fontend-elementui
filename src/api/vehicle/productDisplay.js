import http from '@/utils/request'
import SERVER from '@/api/server'
export function queryProductDisplayInfo(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productDisplay/queryProductDisplayInfo`
  return http.get(url, {
    params: params
  })
}
export function addProductDisplay(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productDisplay/addProductDisplay`
  return http.post(url, params)
}
export function editProductDisplay(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productDisplay/editProductDisplay`
  return http.post(url, params)
}
export const getDetail = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productDisplay/getDetail/${id}`
  return http.get(url)
}
