import http from '@/utils/request'
import SERVER from '@/api/server'

export function queryPageInfos4dealer(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/dealer/queryPageInfos`
  return http.get(url, {
    params: params
  })
}

export function queryInfos4dealer(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/dealer/queryInfos`
  return http.get(url, {
    params: params
  })
}

export function updateStatus4dealer(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/dealer/updateStatus`
  return http.post(url, params)
}

export function getConfImg() {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/dealer/getConfImg`
  return http.get(url)
}

export function updateConfImg(params = {}) {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/dealer/updateConfImg`
  return http.post(url, params)
}
