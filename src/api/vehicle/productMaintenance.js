import request from '@/utils/request'
import SERVER from '@/api/server'

// 查询订购协议列表
export const getProductList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productMaintenance`
  return request.get(url, {
    params: params
  })
}
// 新增订购协议
export const addNewProduct = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productMaintenance/addProduct`
  return request.post(url, params)
}
// 编辑订购协议
export const editProduct = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productMaintenance/editProduct`
  return request.post(url, params)
}
// 根据id查订协议
export const selectById = (id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productMaintenance/${id}`
  return request.get(url, { id: id })
}

// 查询关系列表
export const getSeriesRelList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productMaintenance/getSeriesRelList`
  return request.get(url, {
    params: params
  })
}
// 查询车系列表
export const getSeriesList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productMaintenance/getSeriesList`
  return request.get(url, {
    params: params
  })
}
// 新增关系
export const addProductRel = (params = {}, id) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productMaintenance/addProductRel/${id}`
  return request.post(url, params, id)
}
// 查询全部品牌
export const selectBrandList = () => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/productMaintenance/selectBrandList`
  return request.get(url)
}
