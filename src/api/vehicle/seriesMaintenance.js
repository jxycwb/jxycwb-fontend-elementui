import request from '@/utils/request'
import SERVER from '@/api/server'

// 查询订购协议列表
export const getSeriesList = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/seriesMaintenance`
  return request.get(url, {
    params: params
  })
}
// 查询车系
export const findProduct = () => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/seriesMaintenance/findProduct`
  return request.get(url)
}
// 编辑
export const editSeries = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/seriesMaintenance/editSeries`
  return request.post(url, params)
}
// 查询dms_series
export const selectDmsSeries = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/seriesMaintenance/selectDmsSeries`
  return request.get(url, {
    params: params
  })
}
// 新增
export const addNewSeries = (params = {}) => {
  const url = `${SERVER.DMSCLOUD_VEHICLE}/seriesMaintenance/addSeries`
  return request.post(url, params)
}

// 同步选配器
export const synDmsBrandAndSeries = () => {
  const url = `${SERVER.DMSCLOUD_INTERFACEPLATFORM}/home/synDmsBrandAndSeries`
  return request.get(url)
}

