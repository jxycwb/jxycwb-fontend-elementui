import constant from '@/assets/js/constant';

export default () => {
  const BMap_URL = "http://api.map.baidu.com/api?v=2.0&ak=" + constant.AK + "&s=1&callback=onBMapCallback";
  return new Promise((resolve, reject) => {
    if (typeof BMap !== 'undefined') {
      resolve(BMap)
      return true
    }
    window.onBMapCallback = function () {
      log('百度地图初始化成功')
      resolve(BMap);
    }
    let script = document.createElement("script");
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', BMap_URL);
    script.onerror = reject;
    document.body.appendChild(script);
  })
}
