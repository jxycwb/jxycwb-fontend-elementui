import Vue from 'vue'
import store from '@/store'
Vue.directive('has', {
  inserted(el, binding) {
    const auth = store.getters.authHandle
    var {
      value,
      arg
    } = binding

    if (arg === 'auth' && typeof value === 'string') {
      // 有值的并且为true显示
      if (!(auth.hasOwnProperty(`${value}$`) && auth[`${value}$`])) {
        el.parentNode.removeChild(el)
      }
    } else {
      // 不正确标识
      console.log('不正确标识')
      return false
    }
  }
})
