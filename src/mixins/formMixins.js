import {
  parseTime
} from '@/utils/index'
import {
  mapGetters
} from 'vuex'
export default {
  computed: {
    ...mapGetters(['dictMap'])
  },
  methods: {
    headerCellClassName() {
      return 'baseTable'
    },
    open(key) {
      this.dialog[key] = true
    },
    close(key) {
      this.dialog[key] = false
    },
    // 重置表单并查询
    resetFormNoSearch(name, search = true) {
      this.search = {}
      this.$refs[name] && this.$refs[name].resetFields()
      this.search.pageNum = 1
      this.search.limit = 10
      if (name === 'searchForm' && this.search && this.search.brandOrg) {
        /*
        brand:
        groupCode: DS
        orgName: 南京大道
        pkOrg: 12000
        orgCode: CCN03BT
        * */
        delete this.search.brand
        delete this.search.groupCode
        delete this.search.orgName
        delete this.search.pkOrg
        delete this.search.orgCode
      }
      if (search) this.searchHandle && this.searchHandle()
    },
    // 重置表单不查询
    resetForm(name) {
      this.$refs[name] && this.$refs[name].resetFields()
    },
    // 改变当前也显示数目
    changeSizeHandle(table, size) {
      this.search.limit = size
      this.searchHandle && this.searchHandle()
    },
    // 改变页数
    changePageHandle(table, num) {
      this.search.pageNum = num
      this.searchHandle && this.searchHandle()
    },
    changeNext(val) {
      this.search.pageNum += val
      this.searchHandle && this.searchHandle()
    },
    changePrev(val) {
      this.search.pageNum -= val
      this.searchHandle && this.searchHandle()
    },
    changePage(val, page) {
      this.defaultParams.pageNum = page
      this.searchHandle()
    },
    cancel(dialog) {
      dialog.close()
    },
    // 表格时间格式化
    dateFormat(row, column) {
      const date = row[column.property]
      if (date === undefined) {
        return ''
      }
      return parseTime(date)
    },
    // 表格时间格式化yyyy-MM-dd
    formatDate(row, column) {
      const date = row[column.property]
      if (date === undefined) {
        return ''
      }
      return parseTime(date, '{y}-{m}-{d}')
    },
    // 表格时间格式化yyyy-MM-dd HH:mm
    formatDateM(row, column) {
      const date = row[column.property]
      if (date === undefined) {
        return ''
      }
      return parseTime(date, '{y}-{m}-{d} {h}:{i}')
    },
    // 表格时间格式化yyyy-MM-dd HH:mm
    formatDateS(row, column) {
      const date = row[column.property]
      if (date === undefined) {
        return ''
      }
      return parseTime(date, '{y}-{m}-{d} {h}:{i}:{s}')
    },
    // 表格code过滤
    filterCode(row, column) {
      const code = row[column.property]
      if (code === undefined) {
        return ''
      }
      const value = this.dictMap[column.type]
      if (value) {
        for (let i = 0; i < value.length; i++) {
          if (Number(code) === value[i].code_id) {
            return value[i].code_cn_desc
          }
        }
      } else {
        return code
      }
    },

    // 表格code过滤 多选
    filterCodeMup(row, column) {
      debugger
      const code = row[column.property]
      if (!code) {
        return ''
      }
      const value = this.dictMap[column.type]
      const arr = code.split(',')
      let codeMupStr = ''
      if (value) {
        for (let j = 0; j < arr.length; j++) {
          for (let i = 0; i < value.length; i++) {
            if (Number(arr[j]) === value[i].code_id) {
              codeMupStr += value[i].code_cn_desc + ';'
              break
            }
          }
        }
        return codeMupStr
      } else {
        return codeMupStr
      }
    },

    formatNumber(row, column) {
      let num = row[column.property]
      if (isNaN(num)) { num = '0' }
      return parseFloat(num).toFixed(2)
    },

    // 钱转换带千位符
    formatCurrency(row, column) {
      let num = row[column.property]
      if (num == null || num === '') {
        return ''
      }
      num = num.toString().replace(/\$|\,/g, '')
      if (isNaN(num)) { num = '0' }
      const sign = (num == (num = Math.abs(num)))
      num = Math.floor(num * 100 + 0.50000000001)
      let cents = num % 100
      num = Math.floor(num / 100).toString()
      if (cents < 10) { cents = '0' + cents }
      for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
        num = num.substring(0, num.length - (4 * i + 3)) + ',' +
          num.substring(num.length - (4 * i + 3))
      }
      return (((sign) ? '' : '-') + '' + num + '.' + cents)
    },

    toCamel(str) {
      if (str.indexOf('_')) {
        return str.toLowerCase().replace(/_(\w)/g, ($0, $1) => {
          return $1.toUpperCase()
        })
      } else {
        return str
      }
    },
    convertCamel(arr) {
      arr.forEach(item => {
        for (const k in item) {
          item[this.toCamel(k)] = item[k]
        }
      })
      return arr
    },
    // 列排序
    sortChange(row) {
      debugger
      if (row.order === 'ascending') {
        this.search.order = 'asc'
      }
      if (row.order === 'descending') {
        this.search.order = 'desc'
      }
      this.search.sort = row.prop
      this.searchHandle()
    },
    // 根据code_id获取code_cn_desc
    getDescByCodeId(codeId) {
      if (codeId == null || codeId === '') {
        return ''
      }
      var codeIdStr = codeId + ''
      var codeType = parseInt(codeIdStr.substring(0, 4))
      const value = this.dictMap[codeType]
      for (let i = 0; i < value.length; i++) {
        if (Number(codeId) === value[i].code_id) {
          return value[i].code_cn_desc
        }
      }
    },
    getDescByCodeIds(codeId) {
      if (codeId == null || codeId === '') {
        return ''
      }
      var codeIdStr = codeId + ''
      var codeType = parseInt(codeIdStr.substring(0, 4))
      const value = this.dictMap[codeType]
      for (let i = 0; i < value.length; i++) {
        if (Number(codeId) === value[i].code_id) {
          return value[i].code_cn_desc
        }
      }
    },
    toChies(values) { // 形参
      debugger
      const len = values.length// 统计出长度
      let arr = []
      const chin_list = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖']// 所有的数值对应的汉字
      const chin_lisp = ['仟', '佰', '拾', '万', '仟', '佰', '拾', '', '角', '分']// 进制

      for (let i = 0; i < len; i++) {
        arr.push(parseInt(values[i]))// 输入的数据按下标存进去   存进去的只是数字
        arr[i] = chin_list[arr[i]]// 是根据我们输入的输入的数字，对应着我们的chin_list这个数组
      }// 123['壹','佰','贰','拾','叁']

      for (let i = len - 1, j = 1; i > 0; i--) { // 倒序,为了添加进制，方便我们去观看
        arr.splice(i, 0, chin_lisp[chin_lisp.length - j++]) // j=2
      }
      console.log(arr)

      arr = arr.join('')
      if (len >= 1) {
        arr += '元整'
      }
      return arr
    },
    addNum(num1, num2) {
      let sq1, sq2
      try {
        sq1 = num1.toString().split('.')[1].length
      } catch (e) {
        sq1 = 0
      }
      try {
        sq2 = num2.toString().split('.')[1].length
      } catch (e) {
        sq2 = 0
      }
      const m = Math.pow(10, Math.max(sq1, sq2))
      return (num1 * m + num2 * m) / m
    }
  }
}
