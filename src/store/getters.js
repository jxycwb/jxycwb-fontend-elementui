const getters = {
  sidebar: state => state.app.sidebar,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  userInfo: state => state.user.userInfo,
  roles: state => state.user.roles,
  router: state => state.user.router,
  secondRouter: state => state.user.secondRouter,
  activeRouter: state => state.user.activeRouter,
  isLogin: state => state.user.isLogin,
  errorLogs: state => state.errorLog.logs,
  dictMap: state => state.user.dictMap,
  modelParam: state => state.user.modelParam,
  regionMap: state => state.user.regionMap,
  systemParam: state => state.user.systemParam,
  authHandle: state => state.user.authHandle,
  demo: state => state.demo,
  loginForm: state => state.user.loginForm,
  collectAuthPaths: state => state.user.collectAuthPaths,
  collectIds: state => state.user.collectIds
  // custom: state => state.custom.info
}
export default getters
