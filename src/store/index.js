import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/base/user'
import errorLog from './modules/errorLog'
import tagsView from './modules/tagsView'
import custom from './modules/custom'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    user,
    errorLog,
    tagsView,
    custom
  },
  getters
})

export default store
