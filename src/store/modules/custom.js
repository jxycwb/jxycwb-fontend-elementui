import {
  Message
} from 'element-ui'

const custom = {
  state: {
    // 客户接待
    masterInfo: {
      carInfo: {},
      project: [],
      part: [],
      addOne: [],
      clinet: [],
      salesPart: []
    },
    // 估价单
    valuationInfo: {
      carInfo: {},
      project: [],
      part: [],
      addOne: [],
      result: true
    },
    // 费用结算
    settlementInfo: {
      carInfo: {},
      project: [],
      part: [],
      addOne: [],
      clinet: [],
      salesPart: []
    },
    searchPart: [],
    gmsSearchPart: []
  },
  mutations: {
    // 点击编辑查询之后保存
    saveEditPart(state, data) {
      state.searchPart.push(data)
    },
    // 新建保存 不会重复添加会提示
    saveSearchPart(state, data) {
      const code = data.PART_CODE
      const isRepeat = state.searchPart.some(v => v.PART_CODE === code)
      if (!isRepeat) {
        if (data instanceof Array) {
          state.searchPart.push(...data)
        } else {
          state.searchPart.push(data)
        }
        Message({
          type: 'success',
          message: '添加成功'
        })
      } else {
        Message({
          type: 'warning',
          message: '已经添加过此配件数据'
        })
      }
    },
    // 删除
    deleteSearchPart(state, index) {
      state.searchPart.splice(index, 1)
    },
    // 联动删除
    filterSeachPart(state, code) {
      state.searchPart = state.searchPart.filter(v => {
        if (v.labourCode !== code) {
          return true
        }
      })
    },
    saveSearchPartGMS(state, data) {
      const code = data.PART_CODE
      const isRepeat = state.gmsSearchPart.some(v => v.PART_CODE === code)
      if (!isRepeat) {
        if (data instanceof Array) {
          state.gmsSearchPart.push(...data)
        } else {
          state.gmsSearchPart.push(data)
        }
        Message({
          type: 'success',
          message: '添加成功'
        })
      } else {
        Message({
          type: 'warning',
          message: '已经添加过此配件数据'
        })
      }
    },
    deleteSearchPartGMS(state, index) {
      state.gmsSearchPart.splice(index, 1)
    },
    filterSeachPartGMS(state, code) {
      state.gmsSearchPart = state.gmsSearchPart.filter(v => {
        if (v.labourCode !== code) {
          return true
        }
      })
    },
    saveCarInfo(state, data) {
      state.masterInfo.carInfo.deliverer = ''
      state.masterInfo.carInfo.delivererMobile = ''
      state.masterInfo.carInfo.repairAddress = ''
      state.masterInfo.carInfo = data
    },
    saveProjectInfo(state, data) {
      data.forEach(el => {
        const code = el.labourCode
        const isRepeat = state.masterInfo.project.some(v => v.labourCode === code)
        if (!isRepeat) {
          if (el instanceof Array) {
            state.masterInfo.project.push(...el)
          } else {
            state.masterInfo.project.push(el)
          }
          if (state.masterInfo.result) {
            Message({
              type: 'success',
              message: '添加成功'
            })
          }
        } else {
          Message({
            type: 'warning',
            message: '已经添加过此维修项目'
          })
        }
      })
    },
    savePartInfo(state, data) {
      data.forEach(el => {
        const code = el.labourCode
        const partNo = el.partNo
        const roPartId = el.roPartId
        const isRepeat = state.masterInfo.part.some(v => v.labourCode === code && v.partNo === partNo && v.repeatPart !== 0 && v.partQuantity > 0 && roPartId === undefined)
        if (!isRepeat) {
          if (el instanceof Array) {
            state.masterInfo.part.push(...el)
          } else {
            state.masterInfo.part.push(el)
          }
          if (state.masterInfo.result) {
            Message({
              type: 'success',
              message: '添加成功'
            })
          }
        } else {
          Message({
            type: 'warning',
            message: '已经添加过此配件'
          })
        }
      })
    },
    saveAddOne(state, data) {
      const addItemCode = data.addItemCode
      const isRepeat = state.masterInfo.addOne.some(v => v.addItemCode === addItemCode)
      if (!isRepeat) {
        if (data instanceof Array) {
          state.masterInfo.addOne.push(...data)
        } else {
          state.masterInfo.addOne.push(data)
        }
        if (state.masterInfo.result) {
          Message({
            type: 'success',
            message: '添加成功'
          })
        }
      } else {
        Message({
          type: 'warning',
          message: '已经添加过此附加项目'
        })
      }
    },
    saveClinet(state, data) {
      data.forEach(Item => {
        var key = 1
        if (state.masterInfo.clinet.length > 0) {
          for (var i = 0; i < state.masterInfo.clinet.length; i++) {
            if (state.masterInfo.clinet[i].PAYMENT_OBJECT_CODE === Item.PAYMENT_OBJECT_CODE) {
              key = 0
              break
            }
          }
        }
        if (key === 1) {
          state.masterInfo.clinet.push(Item)
        }
      })
    },
    savePartPales(state, data) {
      data.forEach(Item => {
        state.masterInfo.salesPart.push(Item)
      })
    },
    saveResultValuation(state, data) {
      state.valuationInfo.result = data
    },
    saveCarInfoValuation(state, data) {
      state.valuationInfo.carInfo = data
    },
    saveProjectInfoValuation(state, data) {
      data.forEach(el => {
        const code = el.labourCode
        const isRepeat = state.valuationInfo.project.some(v => v.labourCode === code)
        if (!isRepeat) {
          if (el instanceof Array) {
            state.valuationInfo.project.push(...el)
          } else {
            state.valuationInfo.project.push(el)
          }
          if (state.valuationInfo.result) {
            Message({
              type: 'success',
              message: '添加成功'
            })
          }
        } else {
          Message({
            type: 'warning',
            message: '已经添加过此维修项目'
          })
        }
      })
    },
    savePartInfoValuation(state, data) {
      data.forEach(el => {
        const code = el.labourCode
        const partNo = el.partNo
        const isRepeat = state.valuationInfo.part.some(v => v.labourCode === code && v.partNo === partNo)
        if (!isRepeat) {
          if (el instanceof Array) {
            state.valuationInfo.part.push(...el)
          } else {
            state.valuationInfo.part.push(el)
          }
          if (state.valuationInfo.result) {
            Message({
              type: 'success',
              message: '添加成功'
            })
          }
        } else {
          Message({
            type: 'warning',
            message: '已经添加过此配件'
          })
        }
      })
    },
    saveAddOneValuation(state, data) {
      const addItemCode = data.addItemCode
      const isRepeat = state.valuationInfo.addOne.some(v => v.addItemCode === addItemCode)
      if (!isRepeat) {
        if (data instanceof Array) {
          state.valuationInfo.addOne.push(...data)
        } else {
          state.valuationInfo.addOne.push(data)
        }
        if (state.valuationInfo.result) {
          Message({
            type: 'success',
            message: '添加成功'
          })
        }
      } else {
        Message({
          type: 'warning',
          message: '已经添加过此附加项目'
        })
      }
    },
    // 费用结算
    saveSettlementCarInfo(state, data) {
      state.settlementInfo.carInfo = data
    },
    saveSettlementProjectInfo(state, data) {
      data.forEach(Item => {
        var key = 1
        if (state.settlementInfo.project.length > 0) {
          for (var i = 0; i < state.settlementInfo.project.length; i++) {
            if (state.settlementInfo.project[i].labourCode === Item.labourCode) {
              key = 0
              break
            }
          }
        }
        if (key === 1) {
          state.settlementInfo.project.push(Item)
        }
      })
    },
    saveSettlementPartInfo(state, data) {
      data.forEach(Item => {
        var key = 1
        if (state.settlementInfo.part.length > 0) {
          for (var i = 0; i < state.settlementInfo.part.length; i++) {
            if (state.settlementInfo.part[i].labourCode === Item.labourCode && state.settlementInfo.part[i].partNo === Item.partNo) {
              key = 0
              break
            }
          }
        }
        if (key === 1) {
          state.settlementInfo.part.push(Item)
        }
      })
    },
    saveSettlementAddOne(state, data) {
      data.forEach(Item => {
        var key = 1
        if (state.settlementInfo.addOne.length > 0) {
          for (var i = 0; i < state.settlementInfo.addOne.length; i++) {
            if (state.settlementInfo.addOne[i].addItemCode === Item.addItemCode) {
              key = 0
              break
            }
          }
        }
        if (key === 1) {
          state.settlementInfo.addOne.push(Item)
        }
      })
    },
    saveSettlementClinet(state, data) {
      data.forEach(Item => {
        var key = 1
        if (state.settlementInfo.clinet.length > 0) {
          for (var i = 0; i < state.settlementInfo.clinet.length; i++) {
            if (state.settlementInfo.clinet[i].PAYMENT_OBJECT_CODE === Item.PAYMENT_OBJECT_CODE) {
              key = 0
              break
            }
          }
        }
        if (key === 1) {
          state.settlementInfo.clinet.push(Item)
        }
      })
    },
    saveSettlementPartPales(state, data) {
      data.forEach(Item => {
        state.settlementInfo.salesPart.push(Item)
      })
    },
    clearMasterInfoValuation(state) {
      state.valuationInfo = {
        carInfo: {},
        project: [],
        part: [],
        addOne: [],
        clinet: []
      }
    },
    clearValuation(state) {
      state.valuationInfo.project = []
      state.valuationInfo.part = []
      state.valuationInfo.addOne = []
    },
    clearMasterInfo(state) {
      state.masterInfo = {
        carInfo: {},
        project: [],
        part: [],
        addOne: []
      }
    },
    clearSettlementInfo(state) {
      state.settlementInfo = {
        carInfo: {},
        project: [],
        part: [],
        addOne: [],
        clinet: [],
        salesPart: []
      }
    },
    clear(state) {
      state.masterInfo.project = []
      state.masterInfo.part = []
      state.masterInfo.addOne = []
    }
  }
}

export default custom
