import Car1 from '@/assets/img/car1.png'
import Car2 from '@/assets/img/car2.png'

let BMap = null

export default class Track {
  constructor() {
    this.map = null
    this.coordinate = null
    this.markerArr = null
    this.p_ = []
    this.output_str = ''
    this.reruenPoint = null
    this.nowPoint = null
  }

  static singleton(...args) {
    if (this.instance === undefined) {
      this.instance = new Track(...args)
    }
    return this.instance
  }

  initMap(Bmap, data) {
    BMap = Bmap
    this.map = new BMap.Map('allmap')
    this.draw(data)
  }

  draw(data) {
    this.setPline(data)
    this.createMap()// 创建地图
    this.addMarker()// 添加标记
    this.addPolyline()// 向地图中添加线
  }

  createMap() {
    this.map = new BMap.Map('allmap')
    if (!this.coordinate.length) {
      return
    }
    const [lng, lat] = this.coordinate.split(',')
    let point = new BMap.Point(lng, lat)
    if (this.markerArr) {
      const json = this.markerArr[0]
      if (json) {
        const [p0, p1] = json.point.split('|')
        point = new BMap.Point(p0, p1)
      }
    }
    this.map.centerAndZoom(point, 14)
    this.map.enableScrollWheelZoom(true)
  }

  addMarker() {
    const _this = this
    if (this.markerArr) {
      // let point_1, point_2, point_3, point_4, iconImgx
      this.markerArr.forEach((el, i) => {
        const [p0, p1] = el.point.split('|')
        const point = new BMap.Point(p0, p1)
        /* if (i === 0) {
          point_1 = new BMap.Point(p0, p1)
        } else if (i === 1) {
          point_2 = new BMap.Point(p0, p1)
        } else if (i === 3) {
          point_3 = new BMap.Point(p0, p1)
        } else if (i === 4) {
          point_4 = new BMap.Point(p0, p1)
        }*/
        const iconImg = this.createIcon(el.icon, Car1)
        const iconImg2 = this.createIcon(el.icon, Car2)
        const iconImgx = el.title === '当前位置' ? iconImg2 : iconImg
        const marker = new BMap.Marker(point, {
          icon: iconImgx
        })
        if (el.title === '当前位置') {
          marker.setTop(true)
        }
        // const iw = this.createInfoWindow(i)
        const label = new BMap.Label(el.title, {
          'offset': new BMap.Size(el.icon.lb - el.icon.x + 10, -20)
        })
        this.map.addOverlay(marker)
        label.setStyle({
          border: 'none',
          color: '#333',
          cursor: 'pointer'
        });
        (function() {
          // const index = i
          const _iw = _this.createInfoWindow(i)
          const _marker = marker
          _marker.addEventListener('click', function() {
            this.openInfoWindow(_iw)
          })
          _iw.addEventListener('open', function() {
            // _marker.getLabel().hide();
          })
          _iw.addEventListener('close', function() {
            // _marker.getLabel().show();
          })
          label.addEventListener('click', function() {
            _marker.openInfoWindow(_iw)
          })
          // 循环时，不再画某些
          // if(isInterval_print){
          //   if (!!json.isOpen) {
          //     label.hide();
          //     _marker.openInfoWindow(_iw);
          //   }
          // }
        })()
      })
    }
  }

  addPolyline() {
    this.p_.forEach(el => {
      const points = []
      if (el.points) {
        el.points.forEach(pos => {
          const [p1, p2] = pos.split('|')
          points.push(new BMap.Point(p1, p2))
        })
      }
      const line = new BMap.Polyline(points, {
        strokeStyle: el.style,
        strokeWeight: el.weight,
        strokeColor: el.color,
        strokeOpacity: 1
      })
      this.map.addOverlay(line)
    })
  }

  setPline(data) {
    this.coordinate = data.coordinate || []
    this.markerArr = data.point || []
    const arrive_ = data.arrive// 接单到就位的轨迹
    const await_ = data.await// 就位到开车间的坐标点集合
    const drive_ = data.drive// 开车到完成间的坐标点集合
    // 标注线数组
    const plPoints = [{
      style: 'solid',
      weight: 4,
      color: 'yellow',
      opacity: 0.6,
      points: arrive_
    }]
    const plPoints_await_ = [{
      style: 'solid',
      weight: 4,
      color: 'yellow',
      opacity: 0.6,
      points: await_
    }]
    const plPoints_drive_ = [{
      style: 'solid',
      weight: 4,
      color: 'green',
      opacity: 0.6,
      points: drive_
    }]
    this.p_ = [...plPoints, ...plPoints_await_, ...plPoints_drive_]
  }

  createIcon(json, image) {
    return new BMap.Icon(image,
      new BMap.Size(json.w * 1.5, json.h * 1.5), {
        imageOffset: new BMap.Size(-json.l, -json.t),
        infoWindowOffset: new BMap.Size(json.lb + 5, 1),
        offset: new BMap.Size(json.x, json.h)
      })
  }

  createInfoWindow(i) {
    const json = this.markerArr[i]
    const str = this.output_str ? this.output_str : json.title
    const opts = {
      width: 250, // 信息窗口宽度
      height: 120 // 信息窗口高度
    }
    const iw = new BMap.InfoWindow("<b class='iw_poi_title' title='" +
      json.title + "'><div id='drivingTimeAndmeter' class='iw_poi_content'>" + str + '</div>' +
      "</b><div class='iw_poi_content' style=font-size:12px>" +
      json.content + '</div>' +
      "<div class='iw_poi_content' style=font-size:12px>" +
      json.driverphone + '</div>' +
      "<div class='iw_poi_content' style=font-size:12px>" +
      json.status + '</div>' +
      "<div class='iw_poi_content' style=font-size:12px>" +
      json.time + '</div>', opts
    )
    return iw
  }
}
