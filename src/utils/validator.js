export function max(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  if (parseFloat(value) <= parseFloat(rule.value)) {
    callback()
  } else {
    callback(new Error('请输入不大于' + rule.value + '的数值!'))
  }
}
export function min(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  if (parseFloat(value) >= parseFloat(rule.value)) {
    callback()
  } else {
    callback(new Error('请输入不小于' + rule.value + '的数值!'))
  }
}
export function minGT(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  if (parseFloat(value) > parseFloat(rule.value)) {
    callback()
  } else {
    callback(new Error('请输入大于' + rule.value + '的数值!'))
  }
}
export function maxGT(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  if (parseFloat(value) < parseFloat(rule.value)) {
    callback()
  } else {
    callback(new Error('请输入小于' + rule.value + '的数值!'))
  }
}

export function IDNumber(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  const city = {
    11: '北京',
    12: '天津',
    13: '河北',
    14: '山西',
    15: '内蒙古',
    21: '辽宁',
    22: '吉林',
    23: '黑龙江 ',
    31: '上海',
    32: '江苏',
    33: '浙江',
    34: '安徽',
    35: '福建',
    36: '江西',
    37: '山东',
    41: '河南',
    42: '湖北 ',
    43: '湖南',
    44: '广东',
    45: '广西',
    46: '海南',
    50: '重庆',
    51: '四川',
    52: '贵州',
    53: '云南',
    54: '西藏 ',
    61: '陕西',
    62: '甘肃',
    63: '青海',
    64: '宁夏',
    65: '新疆',
    71: '台湾',
    81: '香港',
    82: '澳门',
    91: '国外 '
  }

  // const tip = ''
  if (!value || !/^\d{15}$/ || !/^\d{6}(18|19|20)\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(value)) {
    callback(new Error('请输入正确的身份证号码!'))
  } else if (!city[value.substr(0, 2)]) {
    callback(new Error('请输入正确的身份证号码!'))
  } else {
    // 18位身份证需要验证最后一位校验位
    if (value.length === 18) {
      value = value.split('')
      // ∑(ai×Wi)(mod 11)
      // 加权因子
      var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
      // 校验位
      var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2]
      var sum = 0
      var ai = 0
      var wi = 0
      for (var i = 0; i < 17; i++) {
        ai = value[i]
        wi = factor[i]
        sum += ai * wi
      }
      // var last = parity[sum % 11]
      if (String(parity[sum % 11]) !== value[17]) {
        callback(new Error('请输入正确的身份证号码!'))
      } else {
        callback()
      }
    } else {
      callback()
    }
  }
}
export const phone = (rule, value, callback, ref) => {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var pattern = /^1[2-9]\d{9}$/
  if (pattern.test(value)) {
    callback()
  }
  callback(new Error('请输入正确的手机号码!'))
}
export function digits(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[1-9][0-9]*$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('只能输入正整数!'))
}

export function numberCheck(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^\-?[0-9]\d*.?\d*$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('只能输入数字!'))
}
export function email(rule, value, callback) {
  if (!value || value == null) {
    callback()
  }
  var g = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
  if (!g.test(value)) {
    callback(new Error('请输入有效的邮件地址!'))
  }
  if (value) {
    if (value.length > 60) {
      callback(new Error('请输入有效的邮件地址!'))
    }
  }
  callback()
}
export function maxDigit(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  // 截取整数
  var digitStr = (value + '').split('.')
  if (digitStr[0].length <= rule.limit) {
    callback()
  }
  callback(new Error('最多输入' + rule.limit + '位整数'))
}
export function money(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[-]?(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入正确的金额(最多2位小数)'))
}
export function moneyMinus(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[-]?(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入正确的金额(最多2位小数)'))
}

export function moneyZS(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入大于0的金额(最多2位小数)'))
}

export function moneyPosition(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入大于等于0的金额(最多2位小数)'))
}

export function decimal(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(([1-9][0-9]*)|(([0]\.\d{1,}|[1-9][0-9]*\.\d{1,})))$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入正确的数值'))
}
export function maxPrecision(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  // 截取整数
  var digitStr = (value + '').split('.')
  if (digitStr.length === 2) {
    if (digitStr[1].length > rule.limit) {
      callback(new Error('最多输入' + rule.limit + '位小数!'))
    }
  }
  callback()
}
export function minPrecision(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  // 截取整数
  var digitStr = (value + '').split('.')
  // eslint-disable-next-line eqeqeq
  if (digitStr.length != 2) {
    callback(new Error('请输入至少5位小数'))
  }
  if (digitStr.length === 2) {
    if (digitStr[1].length < rule.limit) {
      callback(new Error('至少保留' + rule.limit + '位小数!'))
    }
  }
  callback()
}
export function zipCode(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[0-9][0-9]{5}$/
  if (!g.test(value)) {
    callback(new Error('请输入正确的邮政编号!'))
  }
  callback()
}
export function partCode(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[\d|a-zA-Z|/|\\|-]{1,20}$/
  if (!g.test(value)) {
    callback(new Error('请输入正确的配件代码(最大长度20)!'))
  }
  callback()
}
// 限制系统代码
export function systemCode(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[\d|a-zA-Z|-|_]{1,}$/
  if (!g.test(value)) {
    callback(new Error('代码只能为字母数字!'))
  }
  callback()
}

// 限制上装缩写
export function szsxCode(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  debugger
  var g = /^[\d|a-zA-Z|-|_]{1,}$/
  if (!g.test(value)) {
    callback(new Error('上装缩写只能为字母数字!'))
  }
  callback()
}
// 车牌自动转大写
export function systemCodeForCard(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  callback(value = value.toUpperCase())
}
export function fax(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(\d{3,4}-)?\d{7,8}$/
  if (!g.test(value)) {
    // callback(new Error('请输入正确传真(传真格式为:XXX-12345678或XXXX-1234567或XXXX-12345678)!'))
    callback(new Error('请输入正确传真!'))
  }
  callback()
}
export function passWD(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(?!\d+$)(?![a-zA-Z]+$)(?![^(a-zA-Z|\d|\u4E00-\u9FA5)]+$).{8,}$/
  if (!g.test(value)) {
    callback(new Error('长度至少8位;数字,字母,特殊字符至少包含两种!'))
  }
  callback()
}
export function telePhone(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^0\d{2,3}-?\d{7,8}$/
  if (!g.test(value)) {
    callback(new Error('请输入正确的电话号码!'))
  }
  callback()
}
export function mobilePhone(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^0\d{2,3}-?\d{7,8}$/
  // var pattern = /^1[3,4,5,6,7,8]\d{9}$/
  var pattern = /^\d{11}$/
  if (pattern.test(value) || g.test(value)) {
    callback()
  }
  callback(new Error('请输入正确的手机或电话号码!'))
}
// equalTo
export function posInteger(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^\+?[1-9]\d*$/
  if (!g.test(value)) {
    callback(new Error('请输入大于0的整数!'))
  }
  callback()
}
// 判断年份
export function isPositiveYear(rule, value, callback) {
  if (value === null || typeof (value) === 'undefined' || value === '') {
    callback()
  } else if (!isdigits(value)) {
    callback(new Error('必须是正整数'))
  } else if (value.length !== 4) {
    console.log(value.length)
    callback(new Error('长度为4位！'))
  } else {
    callback()
  }
}
/**
 * 只能输入正整数
 */
export const isdigits = (value, ref) => {
  if (typeof (value) === 'undefined') {
    return true
  }
  value = String(value).trim()
  let g = /^[1-9][0-9]*$/
  if (g.test(value)) {
    return true
  }
  g = /^[0]?$/
  if (g.test(value)) {
    return true
  }
  return false
}
// equalPartQuantity

export function checkTaxpayerId(rule, taxpayerId, callback) {
  if (taxpayerId === undefined || taxpayerId === '' || taxpayerId === null) {
    callback()
  }
  if (taxpayerId.length === 15) {
    var addressCode = taxpayerId.substring(0, 6)
    // 校验地址码
    var check = checkAddressCode(addressCode)
    if (!check) {
      callback(new Error('请输入正确的地址码!'))
    }
    // 校验组织机构代码
    var orgCode = taxpayerId.substring(6, 9)
    check = isValidOrgCode(orgCode)
    if (!check) {
      callback(new Error('请输入正确的校验组织机构代码!'))
    }
    callback()
  } else {
    callback(new Error('请输入正确的纳税人识别号!'))
  }
}

/**
 *验证组织机构代码是否合法：组织机构代码为8位数字或者拉丁字母+1位校验码。
 *验证最后那位校验码是否与根据公式计算的结果相符。
 */

export function isValidOrgCode(value) {
  if (value !== '') {
    var part1 = value.substring(0, 8)
    var part2 = value.substring(value.length - 1, 1)
    var ws = [3, 7, 9, 10, 5, 8, 4, 2]
    var str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    var reg = /^([0-9A-Z]){8}$/
    if (!reg.test(part1)) {
      return true
    }
    var sum = 0
    for (var i = 0; i < 8; i++) {
      sum += str.indexOf(part1.charAt(i)) * ws[i]
    }
    var C9 = 11 - (sum % 11)
    var YC9 = part2 + ''
    if (C9 === 11) {
      C9 = '0'
    } else if (C9 === 10) {
      C9 = 'X'
    } else {
      C9 = C9 + ''
    }
    return YC9 !== C9
  }
}

/*
 *校验地址码
 */

export function checkAddressCode(addressCode) {
  var provinceAndCitys = {
    11: '北京',
    12: '天津',
    13: '河北',
    14: '山西',
    15: '内蒙古',
    21: '辽宁',
    22: '吉林',
    23: '黑龙江',
    31: '上海',
    32: '江苏',
    33: '浙江',
    34: '安徽',
    35: '福建',
    36: '江西',
    37: '山东',
    41: '河南',
    42: '湖北',
    43: '湖南',
    44: '广东',
    45: '广西',
    46: '海南',
    50: '重庆',
    51: '四川',
    52: '贵州',
    53: '云南',
    54: '西藏',
    61: '陕西',
    62: '甘肃',
    63: '青海',
    64: '宁夏',
    65: '新疆',
    71: '台湾',
    81: '香港',
    82: '澳门',
    91: '国外'
  }
  var check = /^[1-9]\d{5}$/.test(addressCode)
  if (!check) return false
  if (provinceAndCitys[parseInt(addressCode.substring(0, 2))]) {
    return true
  } else {
    return false
  }
}

//  银行卡号Luhn校验算法
//  luhn校验规则：16位银行卡号（19位通用）:
//  1.将未带校验位的 15（或18）位卡号从右依次编号 1 到 15（18），位于奇数位号上的数字乘以 2。
//  2.将奇位乘积的个十位全部相加，再加上所有偶数位上的数字。
//  3.将加法和加上校验位能被 10 整除。

//  bankno为银行卡号
export function luhnCheck(bankno) {
  var lastNum = bankno.substr(bankno.length - 1, 1) // 取出最后一位（与luhn进行比较）
  var first15Num = bankno.substr(0, bankno.length - 1) // 前15或18位
  var newArr = []
  for (var i = first15Num.length - 1; i > -1; i--) { // 前15或18位倒序存进数组
    newArr.push(first15Num.substr(i, 1))
  }
  var arrJiShu = [] // 奇数位*2的积 <9
  var arrJiShu2 = [] // 奇数位*2的积 >9

  var arrOuShu = [] // 偶数位数组
  for (var j = 0; j < newArr.length; j++) {
    if ((j + 1) % 2 === 1) { // 奇数位
      if (parseInt(newArr[j]) * 2 < 9) {
        arrJiShu.push(parseInt(newArr[j]) * 2)
      } else {
        arrJiShu2.push(parseInt(newArr[j]) * 2)
      }
    } else {
      // 偶数位
      arrOuShu.push(newArr[j])
    }
  }

  var jishu_child1 = [] // 奇数位*2 >9 的分割之后的数组个位数
  var jishu_child2 = [] // 奇数位*2 >9 的分割之后的数组十位数
  for (var h = 0; h < arrJiShu2.length; h++) {
    jishu_child1.push(parseInt(arrJiShu2[h]) % 10)
    jishu_child2.push(parseInt(arrJiShu2[h]) / 10)
  }

  var sumJiShu = 0 // 奇数位*2 < 9 的数组之和
  var sumOuShu = 0 // 偶数位数组之和
  var sumJiShuChild1 = 0 // 奇数位*2 >9 的分割之后的数组个位数之和
  var sumJiShuChild2 = 0 // 奇数位*2 >9 的分割之后的数组十位数之和
  var sumTotal = 0
  for (var m = 0; m < arrJiShu.length; m++) {
    sumJiShu = sumJiShu + parseInt(arrJiShu[m])
  }

  for (var n = 0; n < arrOuShu.length; n++) {
    sumOuShu = sumOuShu + parseInt(arrOuShu[n])
  }

  for (var p = 0; p < jishu_child1.length; p++) {
    sumJiShuChild1 = sumJiShuChild1 + parseInt(jishu_child1[p])
    sumJiShuChild2 = sumJiShuChild2 + parseInt(jishu_child2[p])
  }
  // 计算总和
  sumTotal = parseInt(sumJiShu) + parseInt(sumOuShu) + parseInt(sumJiShuChild1) + parseInt(sumJiShuChild2)

  // 计算luhn值
  var k = parseInt(sumTotal) % 10 === 0 ? 10 : parseInt(sumTotal) % 10
  var luhn = 10 - k

  if (lastNum === luhn) {
    return true
  } else {
    return false
  }
}

// 检查银行卡号
export function CheckBankNo(rule, bankno, callback) {
  if (bankno === '') {
    callback(new Error('请输入银行卡号!'))
  }
  if (bankno.length < 16 || bankno.length > 19) {
    callback(new Error('银行卡号长度必须在16到19之间!'))
  }
  var num = /^\d*$/ // 全数字
  if (!num.exec(bankno)) {
    callback(new Error('银行卡号必须全为数字!'))
  }
  // 开头6位
  var strBin = '10,18,30,35,37,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,58,60,62,65,68,69,84,87,88,94,95,98,99'
  if (strBin.indexOf(bankno.substring(0, 2)) === -1) {
    callback(new Error('银行卡号开头6位不符合规范!'))
  }
  // Luhn校验
  if (!luhnCheck(bankno)) {
    callback(new Error('请输入正确的银行卡号!'))
  }
  callback()
}

export const VIN = (rule, value, callback, ref) => {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  if (value.length !== 17) {
    callback(new Error('请输入正确的VIN!'))
  }

  // eslint-disable-next-line one-var
  var LL = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
    VL = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 7, 9, 2, 3, 4, 5, 6, 7, 8, 9],
    FL = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2],
    rs = 0,
    i, n, d, f, cd, cdv

  for (i = 0; i < 17; i++) {
    f = FL[i]
    d = value.slice(i, i + 1)
    if (i === 8) {
      cdv = d
    }
    if (!isNaN(d)) {
      d *= f
    } else {
      for (n = 0; n < LL.length; n++) {
        if (d.toUpperCase() === LL[n]) {
          d = VL[n]
          d *= f
          if (isNaN(cdv) && n === 8) {
            cdv = LL[n]
          }
          break
        }
      }
    }
    rs += d
  }
  cd = rs % 11
  if (cd === 10) {
    cd = 'X'
  }
  if (cd === cdv) {
    callback()
  }
  // callback(new Error('请输入正确的VIN!'))
}

export function vinCheck(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  if (value.length !== 17) {
    callback(new Error('请输入正确的VIN!'))
  }
  var g = /^[\d|a-zA-Z|-]{17}$/
  if (!g.test(value)) {
    callback(new Error('VIN号只能为字母数字!'))
  }
  callback()
}
export function zipCodeSix(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[0-9][0-9]{5}$/
  if (!g.test(value)) {
    callback(new Error('请输入6位正确的邮政编号!'))
  }
  callback()
}

export function codeAndNum(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[\d|a-zA-Z|-]{1,}$/
  if (!g.test(value)) {
    callback(new Error('代码只能为字母或者数字!'))
  }
  callback()
}

// 行驶证只能13位
export function licenseNum(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[\d|a-zA-Z|-]{13}$/
  if (!g.test(value)) {
    callback(new Error('请输入13位行驶证号码!'))
  }
  callback()
}

// 发票只能8位
export function invoiceNum(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[\d|a-zA-Z|-]{8}$/
  if (!g.test(value)) {
    callback(new Error('请输入8位发票号码!'))
  }
  callback()
}
// 发票只能8位数字
export function invoiceNumHC(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[\d]{8}$/
  if (!g.test(value)) {
    callback(new Error('请输入8位数字发票号码!'))
  }
  callback()
}

export function basedataCode(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(?![^a-zA-Z]+$)(?!\D+$)/
  if (!g.test(value)) {
    callback(new Error('渠道细分代码只能为英文+数字!'))
  }
  callback()
}

export function licenseCheck(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  if (value.length !== 8) {
    callback(new Error('请输入正确的车牌号!'))
  }
  var g = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF])|([DF][A-HJ-NP-Z0-9][0-9]{4}))$/
  if (!g.test(value)) {
    callback(new Error('请输入正确的车牌号!'))
  }
  callback()
}
// 限制编码
export function codeCheck(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[\d|a-zA-Z_|-]{1,}$/
  if (!g.test(value)) {
    callback(new Error('代码只能为字母数字!'))
  }
  callback()
}

export const checkNumber = (rule, value, callback) => {
  if (!value) {
    callback()
  } else
  if (!onlyNumber(value)) {
    callback(new Error('必须是正数,精确到小数点后面两位'))
  } else if (value.length > 10) {
    callback(new Error('长度不能超过10位！'))
  } else {
    callback()
  }
}

export const checkRejectRate = (rule, value, callback) => {
  if (!value) {
    callback()
  } else if (!onlyNumber(value)) {
    callback(new Error('必须是正数,精确到小数点后面两位'))
  } else if (parseFloat(value) >= 100) {
    callback(new Error('数值不能大于100'))
  } else {
    callback()
  }
}

export const onlyNumber = (str) => {
  if (!str) {
    return true
  }
  // let numberReg = /^(([0-9]+)|([0-9]+\.[0-9]{1,2}))$/;
  const numberReg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/
  return numberReg.test(str)
}

export function phoneCode(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[0-9]{11}$/
  if (!g.test(value)) {
    callback(new Error('请输入正确的手机号!'))
  }
  callback()
}

// 配件数量校验, 允许0
export function partQuantity(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入正确配件数量(最多2位小数)'))
}

// 配件数量校验, 不允许0
export function partQuantity2(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入正确配件数量(最多2位小数)'))
}

// 数值校验，允许两位小数，允许0
export function numberValueCheck(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入正确的数值(最多2位小数)'))
}
// 年月校验[例：201901]
export function date(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^\d{4}((0([1-9]))|(1(0|1|2)))$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入正确的年月(例：201901)'))
}
export function longitude(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  } else {
    var g = /^[\-\+]?(0(\.\d{1,15})?|([1-9](\d)?)(\.\d{1,15})?|1[0-7]\d{1}(\.\d{1,15})?|180(\.0{1,15})?)$/
    if (!g.test(value)) {
      callback(new Error('请输入正确的经度(最多15位小数)'))
    } else if (value.indexOf('.') > -1) {
      g = /^[0]*$/
      if (g.test(value.replace('.', ''))) {
        callback(new Error('请输入正确的经度(最多15位小数)'))
      }
    } else { callback() }
  }
}

export function latitude(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
    return
  }
  var g = /^[\-\+]?((0|([1-8]\d?))(\.\d{1,15})?|90(\.0{1,15})?)$/
  if (!g.test(value)) {
    callback(new Error('请输入正确的纬度(最多15位小数)'))
    return
  }
  if (value.indexOf('.') > -1) {
    g = /^[0]*$/
    if (g.test(value.replace('.', ''))) {
      callback(new Error('请输入正确的纬度(最多15位小数)'))
      return
    }
  }
  callback()
}

export function CheckSocialCreditCode(rule, value, callback) {
  var patrn = /^[0-9A-Z]+$/
  // 18位校验及大写校验
  if ((value.length !== 18) || (patrn.test(value) === false)) {
    callback(new Error('不是有效的统一社会信用编码！'))
  } else {
    var Ancode // 统一社会信用代码的每一个值
    var Ancodevalue // 统一社会信用代码每一个值的权重
    var total = 0
    var weightedfactors = [1, 3, 9, 27, 19, 26, 16, 17, 20, 29, 25, 13, 8, 24, 10, 30, 28] // 加权因子
    var str = '0123456789ABCDEFGHJKLMNPQRTUWXY'
    // 不用I、O、S、V、Z
    for (var i = 0; i < value.length - 1; i++) {
      Ancode = value.substring(i, i + 1)
      Ancodevalue = str.indexOf(Ancode)
      total = total + Ancodevalue * weightedfactors[i]
      // 权重与加权因子相乘之和
    }
    var logiccheckcode = 31 - total % 31
    if (logiccheckcode === 31) {
      logiccheckcode = 0
    }
    var Str = '0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,J,K,L,M,N,P,Q,R,T,U,W,X,Y'
    var Array_Str = Str.split(',')
    logiccheckcode = Array_Str[logiccheckcode]

    var checkcode = value.substring(17, 18)
    if (logiccheckcode !== checkcode) {
      callback(new Error('不是有效的统一社会信用编码！'))
    }
    callback()
  }
}

export function checkLandline(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^[0-9,-]*$/
  if (!g.test(value)) {
    callback(new Error('请输入正确的电话号码!'))
  }
  callback()
}

export function digitsFlag(rule, value, callback) {
  if (value === undefined || value === '' || value === null) {
    callback()
  }
  var g = /^(\d{1,4})$/
  if (g.test(value)) {
    callback()
  }
  g = /^[0]?$/
  if (g.test(value)) {
    callback()
  }
  callback(new Error('请输入正整数(最多4位)'))
}

export function checkBlank(rule, value, callback) {
  if (value === undefined || value === '' || value === null || value === ' ') {
    callback(new Error('请选择'))
  }
}
