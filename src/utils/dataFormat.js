// 字符串的下划线格式转驼峰格式，eg：HEllO_WORD => helloWorld
export function underline2Hump(s) {
  if (s.indexOf('_') !== -1) {
    return s.toLowerCase().replace(/_(\w)/g, function(all, letter) {
      return letter.toUpperCase()
    })
  } else {
    // 全部大写才转化小写输出
    if (/^[0-9A-Z]*$/.test(s)) {
      return s.toLowerCase()
    } else {
      return s
    }
  }
}

// 字符串的驼峰格式转下划线格式，eg：helloWorld => HELLO_WORD
export function hump2Underline(s) {
  return s.replace(/([A-Z])/g, '_$1').toUpperCase()
}

// JSON对象的key值转换为驼峰式
export function jsonToHump(obj) {
  const newObj = {}
  if (obj instanceof Array) {
    obj.forEach(function(v, i) {
      jsonToHump(v)
    })
  } else if (obj instanceof Object) {
    Object.keys(obj).forEach(function(key) {
      var newKey = underline2Hump(key)
      if (newKey !== key) {
        newObj[newKey] = obj[key]
      }
      jsonToHump(obj[newKey])
    })
  }
  return newObj
}

// JSON对象的key值转换为下划线格式
export function jsonToUnderline(obj) {
  if (obj instanceof Array) {
    obj.forEach(function(v, i) {
      jsonToUnderline(v)
    })
  } else if (obj instanceof Object) {
    Object.keys(obj).forEach(function(key) {
      var newKey = hump2Underline(key)
      if (newKey !== key) {
        obj[newKey] = obj[key]
        delete obj[key]
      }
      jsonToUnderline(obj[newKey])
    })
  }
  return obj
}
