// Author:huangwuyi
// Date:2019-11-14
import { Message } from 'element-ui'
export function message_success(msg) {
  Message({
    type: 'success',
    message: msg
  })
  console.log(1)
}

export function message_error(msg) {
  Message({
    type: 'error',
    message: msg
  })
}

export function message_info(msg) {
  Message({
    type: 'info',
    message: msg
  })
}
