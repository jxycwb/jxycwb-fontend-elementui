import store from '../store'

export function formatChineseNumeral(value) {
  if (!/^(0|[1-9]\d*)(\.\d+)?$/.test(value)) {
    return
  }
  let unit = '仟佰拾亿仟佰拾万仟佰拾元角分'
  let str = ''
  value += '00'
  const p = value.indexOf('.')
  if (p >= 0) { value = value.substring(0, p) + value.substr(p + 1, 2) }
  unit = unit.substr(unit.length - value.length)
  for (var i = 0; i < value.length; i++) {
    str += '零壹贰叁肆伍陆柒捌玖'.charAt(value.charAt(i)) + unit.charAt(i)
  }
  return str.replace(/零(仟|佰|拾|角)/g, '零').replace(/(零)+/g, '零').replace(/零(万|亿|元)/g, '$1').replace(/(亿)万/g, '$1$2').replace(/^元零?|零分/g, '').replace(/元$/g, '元整')
}

export function hasMenu(menuId) {
  const router = store.getters.router
  let hasMenu = false
  router.some(item => {
    if (item.children) {
      item.children.some(child => {
        if (child.children) {
          child.children.some(c => {
            if (c.id === menuId) {
              hasMenu = true
              return true
            }
          })
        }
      })
    }
  })
  return hasMenu
}
