import _ from 'lodash'
import Vue from 'vue'
import VeeValidate, { Validator } from 'vee-validate'
import zh from 'vee-validate/dist/locale/zh_CN'// 引入中文文件

/* 合法uri*/
export function validateURL(textval) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return urlregex.test(textval)
}

/* 小写字母*/
export function validateLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/* 大写字母*/
export function validateUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/* 大小写字母*/
export function validatAlphabets(str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}

export const isMobile = (str) => {
  if (_.isEmpty(str)) {
    return false
  }

  const phoneReg = /^0\d{2,3}-?\d{7,8}$/
  if (!phoneReg.test(str)) {
    return false
  }
  return true
}

export const isphone = (str) => {
  if (_.isEmpty(str)) {
    return false
  }

  const phoneReg = /^1[345678]\d{9}$/
  if (!phoneReg.test(str)) {
    return false
  }
  return true
}

export const isPlateNumber = (str) => {
  const plantNumberRex = /^[\u4e00-\u9fa5]{1}[A-Z]{1}[A-Z_0-9]{5}$/
  if (_.isEmpty(str)) {
    return false
  }
  return plantNumberRex.test(str)
}

export const isEmail = (str) => {
  const emailReg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/
  if (!emailReg.test(str)) {
    return false
  }
  return true
}

export const isNumber = (str) => {
  if (!str) {
    return false
  }
  const numberReg = /^[1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0$/
  return numberReg.test(str)
}

export const isSBH = n => {
  const l = n.length
  return (l === 0 || l === 15 || l === 18 || l === 20)
}

export const isBlankNo = n => {
  const l = n.length
  return (l === 0 || l === 19 || l === 16)
}

export const onlyNumber = (str) => {
  if (!str) {
    return true
  }
  // let numberReg = /^(([0-9]+)|([0-9]+\.[0-9]{1,2}))$/;
  const numberReg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/
  return numberReg.test(str)
}

export const onlyLessThanOne = (str) => {
  if (!str) {
    return true
  }
  const numberReg = /^0\.[0-9]{1,2}$|^0{1}$|^1{1}$|^1\.[0]{1,2}$/
  return numberReg.test(str)
}

export const onlyLessThanOneDouble = (rule, value, callback) => {
  if (value === null || typeof (value) === 'undefined' || value === '') {
    callback()
  } else if (!onlyLessThanOne(value)) {
    callback(new Error('必须是小于1的,精确到小数点后面两位'))
  } else {
    callback()
  }
}

export const checkNumber = (rule, value, callback) => {
  if (value === null || typeof (value) === 'undefined' || value === '') {
    callback()
  } else
  if (!onlyNumber(value)) {
    callback(new Error('必须是正数,精确到小数点后面两位'))
  } else if (value.length > 10) {
    callback(new Error('长度不能超过10位！'))
  } else {
    callback()
  }
}
export const isPositiveNumber = (rule, value, callback) => {
  if (value === null || typeof (value) === 'undefined' || value === '') {
    callback()
  } else if (!isdigits(value)) {
    callback(new Error('必须是正整数'))
  } else if (value.length > 10) {
    callback(new Error('长度不能超过10位！'))
  } else {
    callback()
  }
}

export const isVin = (rule, value, callback) => {
  if (value.length !== 17) {
    callback(new Error('VIN格式不正确'))
  } else {
    callback()
  }
}

export const onlyDoubleNumber = (str) => {
  if (!str) {
    return true
  }
  // let numberReg = /^(([0-9]+)|([0-9]+\.[0-9]{1,2}))$/;
  const numberReg = /^0\.[1-9]{0,2}$/
  return numberReg.test(str)
}

export const checkDoubleNumber = (rule, value, callback) => {
  if (!onlyDoubleNumber(value)) {
    callback(new Error('必须是正浮点数,精确到小数点后两位'))
  } else {
    callback()
  }
}

export const isMaxLength = (str, ref) => {
  if (str.length > ref) {
    return false
  }
  return true
}

export const isMinLength = (str, ref) => {
  if (str.length < ref) {
    return false
  }
  return true
}

export const isBetween = (str, ref) => {
  if (str > ref[0] && str < ref[1]) {
    return true
  }
  return false
}

export const isBetweenLength = (str, ref) => {
  if (str.length > ref[0] && str.length < ref[1]) {
    return true
  }
  return false
}

export const isIDNumber = (code) => {
  const city = { 11: '北京', 12: '天津', 13: '河北', 14: '山西', 15: '内蒙古', 21: '辽宁', 22: '吉林', 23: '黑龙江 ', 31: '上海', 32: '江苏', 33: '浙江', 34: '安徽', 35: '福建', 36: '江西', 37: '山东', 41: '河南', 42: '湖北 ', 43: '湖南', 44: '广东', 45: '广西', 46: '海南', 50: '重庆', 51: '四川', 52: '贵州', 53: '云南', 54: '西藏 ', 61: '陕西', 62: '甘肃', 63: '青海', 64: '宁夏', 65: '新疆', 71: '台湾', 81: '香港', 82: '澳门', 91: '国外 ' }

  if (!code || !/^\d{15}$/ || !/^\d{6}(18|19|20)\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)) {
    return false
  } else if (!city[code.substr(0, 2)]) {
    return false
  } else {
    // 18位身份证需要验证最后一位校验位
    if (code.length === 18) {
      code = code.split('')
      // ∑(ai×Wi)(mod 11)
      // 加权因子
      var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
      // 校验位
      var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2]
      var sum = 0
      var ai = 0
      var wi = 0
      for (var i = 0; i < 17; i++) {
        ai = code[i]
        wi = factor[i]
        sum += ai * wi
      }
      if (parity[sum % 11] !== code[17]) {
        return false
      }
    }
  }
  return true
}

export const isVIN = (code) => {
  if (code.length !== 17) {
    return false
  }

  const LL = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  const VL = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 7, 9, 2, 3, 4, 5, 6, 7, 8, 9]
  const FL = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2]
  let rs = 0
  let i, n, d, f, cd, cdv

  for (i = 0; i < 17; i++) {
    f = FL[i]
    d = code.slice(i, i + 1)
    if (i === 8) {
      cdv = d
    }
    if (!isNaN(d)) {
      d *= f
    } else {
      for (n = 0; n < LL.length; n++) {
        if (d.toUpperCase() === LL[n]) {
          d = VL[n]
          d *= f
          if (isNaN(cdv) && n === 8) {
            cdv = LL[n]
          }
          break
        }
      }
    }
    rs += d
  }
  cd = rs % 11
  if (cd === 10) {
    cd = 'X'
  }
  if (cd === cdv) {
    return true
  }
  return false
}

export const iszipCode = (value) => {
  value = value.trim()
  var g = /^[0-9][0-9]{5}$/
  if (!g.test(value)) {
    return false
  }
  return true
}
/**
 * 只能输入正整数
 */
export const isdigits = (value, ref) => {
  if (typeof (value) === 'undefined') {
    return true
  }
  value = String(value).trim()
  let g = /^[1-9][0-9]*$/
  if (g.test(value)) {
    return true
  }
  g = /^[0]?$/
  if (g.test(value)) {
    return true
  }
  return false
}
/**
* 限制金额/价格(只能数入正数)
*/
export const ismoney = (value) => {
  value = value.trim()
  let g = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    return true
  }
  g = /^[0]?$/
  if (g.test(value)) {
    return true
  }
}
/**
* 限制金额/价格(正负数)
*/
export const ismoneyMinus = (value) => {
  value = value.trim()
  let g = /^[-]?(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/
  if (g.test(value)) {
    return true
  }
  g = /^[0]?$/
  if (g.test(value)) {
    return true
  }
}
/**
* 限制金额/价格
*/
export const isdecimal = (value) => {
  value = value.trim()
  let g = /^(([1-9][0-9]*)|(([0]\.\d{1,}|[1-9][0-9]*\.\d{1,})))$/
  if (g.test(value)) {
    return true
  }
  g = /^[0]?$/
  if (g.test(value)) {
    return true
  }
  return false
}
/**
* 限制整数位数
*/
export const ismaxDigit = (value, ref) => {
  value = value.trim()
  const digitStr = value.split('.')
  if (digitStr[0].length > ref) {
    return false
  }
  return true
}

/**
* 限制配件代码
*/
export const ispartCode = (value) => {
  value = value.trim()
  const g = /^[\d|a-zA-Z|/|\\|-]{1,20}$/
  if (!g.test(value)) {
    return false
  }
  return true
}
/**
 *车牌自动转大写
 */

/**
* 限制传真
*/
export const isfax = (value) => {
  value = value.trim()
  const g = /^(\d{3,4}-)?\d{7,8}$/
  if (!g.test(value)) {
    return false
  }
  return true
}

/**
* 密码规则
* 1、长度至少8位；
 * 2、数字、字母、字符至少包含两种。
*/
export const ispassWD = (value) => {
  value = value.trim()
  const g = /^(?!\d+$)(?![a-zA-Z]+$)(?![^(a-zA-Z|\d|\u4E00-\u9FA5)]+$).{8,}$/
  if (!g.test(value)) {
    return false
  }
  return true
}
/**
 * 限制手机或电话号
 */
export const ismobilePhone = (value) => {
  value = value.trim()
  const g = /^0\d{2,3}-?\d{7,8}$/
  const pattern = /^1[345678]\d{9}$/
  if (pattern.test(value) || g.test(value)) {
    return true
  }
  return false
}
/**
 * 正整数 >0的整数
 */
export const isposInteger = (value) => {
  value = value.trim()
  const g = /^\+?[1-9]\d*$/
  if (!g.test(value)) {
    return false
  }
  return true
}

// 配置中文
Validator.localize(zh)

const config = {
  locale: 'zh_CN',
  fieldsBagName: 'fieldBags'
}
Vue.use(VeeValidate, config)

const dictionary = {
  zh_CN: {
    messages: {
      required: (element) => `不能为空!`
    },
    attributes: {
      phone: '手机号码',
      email: '邮箱',
      mobile: '电话',
      max: '最大值',
      min: '最小值',
      maxGT: '最大值',
      minGT: '最小值',
      between: '最大值最小值范围',
      maxlength: '文档最大长度',
      minlength: '文档最短长度',
      betweenLength: '文档长度范围',
      IDNumber: '身份证号',
      decimal: '限制金额/价格',
      VIN: 'VIN',
      zipCode: '邮编',
      digits: '正整数',
      maxDigit: '最多输入{0}位整数',
      moneyMinus: '限制金额/价格(正负数)',
      money: '限制金额/价格(只能数入正数)',
      partCode: '配件代码',
      passWD: '密码',
      fax: '传真',
      maxPrecision: '最多输入{0}位小数',
      systemCode: '系统代码',
      telePhone: '电话规则',
      mobilePhone: '限制手机或电话号',
      posInteger: '大于0的整数'
    }
  }
}

// 引用上述设置
Validator.localize(dictionary)

// 邮箱
Validator.extend('email', {
  validate: (value, ref) => {
    return isEmail(value)
  },
  getMessage: () => '请填写正确的邮箱'
})

// 最大值
Validator.extend('max', {
  validate: (value, ref) => {
    value = value.trim()
    return Number(value) <= Number(ref)
  },
  getMessage: (value, ref) => '请输入不大于' + ref + '的数值'
})

// 最小值
Validator.extend('min', {
  validate: (value, ref) => {
    value = value.trim()
    return Number(value) >= Number(ref)
  },
  getMessage: (value, ref) => '请输入不小于' + ref + '的数值'
})
// maxGT
Validator.extend('maxGT', {
  validate: (value, ref) => {
    value = value.trim()
    return Number(value) < Number(ref)
  },
  getMessage: (value, ref) => '请输入小于' + ref + '的数值'
})

// minGT
Validator.extend('minGT', {
  validate: (value, ref) => {
    value = value.trim()
    return Number(value) > Number(ref)
  },
  getMessage: (value, ref) => '请输入大于' + ref + '的数值'
})

// 范围
Validator.extend('between', {
  validate: (value, ref) => {
    return isBetween(value, ref)
  },
  getMessage: () => '超出范围值'
})
// 电话号码

Validator.extend('mobile', {
  validate: (value, ref) => {
    return isMobile(value)
  },
  getMessage: () => '请填写正确的电话号码'
})
Validator.extend('phone', {
  validate: (value, ref) => {
    return isphone(value)
  },
  getMessage: () => '请填写正确的手机号码'
})

// 文档最大长度
Validator.extend('maxlength', {
  validate: (value, ref) => {
    return isMaxLength(value, ref)
  },
  getMessage: (value, ref) => '最多只能输入 ' + ref + '个字符'
})

// 文档最小长度
Validator.extend('minlength', {
  validate: (value, ref) => {
    return isMinLength(value, ref)
  },
  getMessage: (value, ref) => '最少要输入 ' + ref + ' 个字符'
})

// 文档最小长度
Validator.extend('betweenLength', {
  validate: (value, ref) => {
    return isBetweenLength(value, ref)
  },
  getMessage: () => '超出文档长度范围'
})

// 身份证
Validator.extend('IDNumber', {
  validate: (value, ref) => {
    return isIDNumber(value)
  },
  getMessage: () => '身份证输入错误'
})

// 限制金额/价格
Validator.extend('decimal', {
  validate: (value, ref) => {
    return isdecimal(value)
  },
  getMessage: () => '请输入正确的数值'
})
// vin
Validator.extend('VIN', {
  validate: (value, ref) => {
    return isVIN(value)
  },
  getMessage: () => '请输入正确的VIN'
})
// 邮编
Validator.extend('zipCode', {
  validate: (value, ref) => {
    return iszipCode(value)
  },
  getMessage: () => '请输入正确的邮政编号'
})
// 限制整数位数
Validator.extend('maxDigit', {
  validate: (value, ref) => {
    return ismaxDigit(value, ref)
  },
  getMessage: (value, ref) => '最多输入' + ref + '位整数'
})
// 正整数
Validator.extend('digits', {
  validate: (value, ref) => {
    return isdigits(value, ref)
  },
  getMessage: (value, ref) => '只能输入正整数'
})
// 正整数
Validator.extend('money', {
  validate: (value, ref) => {
    return ismoney(value, ref)
  },
  getMessage: (value, ref) => '请输入正确的金额(最多2位小数)'
})
// 正整数
Validator.extend('moneyMinus', {
  validate: (value, ref) => {
    return ismoneyMinus(value, ref)
  },
  getMessage: (value, ref) => '请输入正确的金额(最多2位小数)'
})

Validator.extend('maxPrecision', {
  validate: (value, ref) => {
    value = value.trim()
    // 截取整数
    var digitStr = value.split('.')
    if (digitStr.length === 2) {
      if (digitStr[1].length > ref) {
        return false
      }
    }
    return true
  },
  getMessage: (value, ref) => '最多输入' + ref + '位小数'
})

Validator.extend('partCode', {
  validate: (value, ref) => {
    return ispartCode(value)
  },
  getMessage: () => '请输入正确的配件代码(最大长度20)'
})
Validator.extend('fax', {
  validate: (value, ref) => {
    return isfax(value)
  },
  getMessage: () => '请输入正确传真(传真格式为:XXX-12345678或XXXX-1234567或XXXX-12345678)'
})
Validator.extend('passWD', {
  validate: (value, ref) => {
    return ispassWD(value)
  },
  getMessage: () => '长度至少8位;数字,字母,特殊字符至少包含两种'
})
Validator.extend('telePhone', {
  validate: (value, ref) => {
    const g = /^0\d{2,3}-?\d{7,8}$/
    if (!g.test(value)) {
      return false
    }
    return true
  },
  getMessage: () => '请输入正确的电话号码'
})
Validator.extend('mobilePhone', {
  validate: (value, ref) => {
    return ismobilePhone(value)
  },
  getMessage: () => '请输入正确的手机或电话号码'
})
Validator.extend('posInteger', {
  validate: (value, ref) => {
    return isposInteger(value)
  },
  getMessage: () => '请输入大于0的整数'
})
