import Mock from 'mockjs'

const data = (pageNum) => {
  return {
    pageSize: 5,
    pageNum: pageNum,
    total: function() {
      return this.pageSize * this.pageNum
    },
    'list|5': [
      {
        'pkRoleTypeId|+1': 1,
        'roleTypeCode': '@string',
        'roleTypeName|1': [
          '测试角色类型',
          '平台555',
          '经销商商户',
          '客服',
          '渠道'
        ],
        'remark': null,
        'status': 10121001,
        'creator': 'Admin',
        'creationtime': 1501667782000,
        'modifier': 1,
        'modifiedtime': 1501667901000
      }
    ]
  }
}

/*
* 角色类型维护
* */

Mock.mock('/init/roleTypeList', 'post', {
  code: 'S',
  data: data(1),
  message: 'success'
})
