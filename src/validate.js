import Vue from 'vue'
import VeeValidate, { Validator } from 'vee-validate'
import * as tools from '@/utils/validate'
const dictionary = {
  en: {
    messages: {
      // <input type="text" v:model="phoneNumber" name="phone" v:validate="'required|phone'">
      // 不满足required条件时，显示的提示对应下面的设置
      required: () => `不能为空!`
    },
    attributes: {
      // <input type="text" v:model="phoneNumber" name="phone" v:validate="'required|phone'">
      // 上述messages提示信息中field显示的内容在此设置，对应标签的name属性
      phone: '手机号码',
      email: '邮箱',
      mobile: '电话',
      max: '最大值',
      min: '最小值',
      between: '最大值最小值范围',
      maxlength: '文档最大长度',
      minlength: '文档最短长度',
      betweenLength: '文档长度范围',
      IDNumber: '身份证号',
      decimal: '限制金额/价格'
    }
  }
}
// 限制金额/价格
Validator.extend('decimal', {
  validate: (value, ref) => {
    return tools.isdecimal(value)
  },
  getMessage: () => '请输入正确的数值'
})

// 引用上述设置
Validator.localize(dictionary)

// 邮箱
Validator.extend('email', {
  validate: (value, ref) => {
    return tools.isEmail(value)
  },
  getMessage: () => '请填写正确的邮箱'
})

// 最大值
Validator.extend('max', {
  validate: (value, ref) => {
    return value < ref
  },
  getMessage: () => '超出最大值'
})

// 最小值
Validator.extend('max', {
  validate: (value, ref) => {
    return value > ref
  },
  getMessage: () => '超出最小值'
})

// 范围
Validator.extend('between', {
  validate: (value, ref) => {
    return tools.isBetween(value, ref)
  },
  getMessage: () => '超出范围值'
})
// 电话号码

Validator.extend('mobile', {
  validate: (value, ref) => {
    return tools.isMobile(value)
  },
  getMessage: () => '请填写正确的电话号码'
})
// 文档最大长度
Validator.extend('maxlength', {
  validate: (value, ref) => {
    return tools.isMaxLength(value, ref)
  },
  getMessage: () => '超出文档最大长度'
})

// 文档最小长度
Validator.extend('minlength', {
  validate: (value, ref) => {
    return tools.isMinLength(value, ref)
  },
  getMessage: () => '超出文档最小长度'
})

// 文档最小长度
Validator.extend('betweenLength', {
  validate: (value, ref) => {
    return tools.isBetweenLength(value, ref)
  },
  getMessage: () => '超出文档长度范围'
})

// 身份证
Validator.extend('IDNumber', {
  validate: (value, ref) => {
    return tools.isIDNumber(value)
  },
  getMessage: () => '身份证输入错误'
})

Vue.use(VeeValidate, { events: 'blur' })
