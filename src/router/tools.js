import store from '@/store'
const lazy = require('./import_' + process.env.NODE_ENV)
export const generaMenu = (data, routers, parentPath = '') => {
  store.dispatch('addCollectAuthPathId', '/dashboard')
  store.dispatch('addCollectAuthPathId', '/settings')

  data.forEach((item) => {
    const menu = Object.assign({}, item)
    if (item.path !== '*') {
      try {
        menu.component = lazy(menu.component)
      } catch (e) {
        console.log(e)
        menu.component = lazy('404')
      }
    }
    if (item.children && item.children.length > 0) {
      menu.children = []
      generaMenu(item.children, menu.children, menu.path)
    }
    // 记录收藏
    if (Number(menu.isFavorite) === 10041001) {
      store.dispatch('addCollectId', menu.id)
    }
    if (menu.path) {
      if (menu.path.charAt(0) == '/') {
        store.dispatch('addCollectAuthPathId', menu.path)
      } else {
        store.dispatch('addCollectAuthPathId', `${parentPath}/${menu.path}`)
      }
    }

    routers.push(menu)
  })
}

