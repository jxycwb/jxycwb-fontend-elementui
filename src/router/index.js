import Vue from 'vue'
import Router from 'vue-router'

/* Layout */
import Layout from '../views/layout/Layout'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)
/**
 * hidden: true                  当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面(默认 false)
 * alwaysShow: true              当设置 true 的时候永远会显示根菜单，不设置的情况下只有当子路由个数大于一个时才会显示根菜单
 *                               当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式。只有一个时会将那个子路由当做根路由
 * redirect: noredirect          当设置 noredirect 的时候该路由不会在面包屑导航中出现
 * name:'router-name'            设定路由的名字，一定要填写不然 使用 <keep-alive> 时会出现各种问题
 * meta : {
*    roles: ['admin','editor']  设置该路由进入的权限，支持多个权限叠加
    title: 'title'              设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'            设置该路由的图标
    noCache: true               如果设置为true ,则不会被 <keep-alive> 缓存(默认 false)
  }
 **/
export const constantRouterMap = [{
  path: '/login',
  component: () => import('@/views/login/index'),
  hidden: true
},
{
  path: '/404',
  component: () => import('@/views/404'),
  hidden: true
},
{
  path: '/401',
  component: () => import('@/views/401'),
  hidden: true
},
{
  path: '/success',
  component: () => import('@/views/success'),
  hidden: true
},
{
  path: '/',
  component: Layout,
  redirect: '/dashboard',
  hidden: true,
  children: [{
    path: 'dashboard',
    name: 'Dashboard',
    component: () => import(`@/views/dashboard/index`),
    meta: {
      title: '首页'
    }
  }, {
    path: 'settings',
    name: 'settings',
    component: () => import(`@/views/settings`),
    meta: {
      title: '个人设置'
    }
  }]
}, {
  path: '/spinPicture',
  name: 'spinPicture',
  component: () => import(`@/views/spinPicture/spinPicture`),
  meta: {
    title: '全景图'
  }
},
{
  path: '/weChat/oauth',
  component: () => import(`@/views/weChat/oauth`),
  name: 'Oauth'
}, {
  path: '/weChat/imtool/cusAndCarInfo',
  component: () => import(`@/views/weChat/imtool/cusAndCarInfo`),
  name: 'CusAndCarInfo'
}, {
  path: '/weChat/report/salesAdvisorCustomCount',
  component: () => import(`@/views/weChat/report/salesAdvisorCustomCount`),
  name: 'SalesAdvisorCustomCount'
}, {
  path: '/weChat/report/salesAdvisorCustomDetail',
  component: () => import(`@/views/weChat/report/salesAdvisorCustomDetail`),
  name: 'SalesAdvisorCustomDetail'
}, {
  path: '/weChat/report/servicesAdvisorCustomCount',
  component: () => import(`@/views/weChat/report/servicesAdvisorCustomCount`),
  name: 'ServicesAdvisorCustomCount'
}, {
  path: '/weChat/report/servicesAdvisorCustomDetail',
  component: () => import(`@/views/weChat/report/servicesAdvisorCustomDetail`),
  name: 'ServicesAdvisorCustomDetail'
}
// , {
//   path: '/*',
//   component: () => import(`@/views/404.vue`),
//   name: '404Page'
// }

]

export default new Router({
  mode: 'history', // 后端支持可开
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
})
