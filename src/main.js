import 'babel-polyfill'
import Vue from 'vue'

import promise from 'es6-promise'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'font-awesome/css/font-awesome.min.css'

import plTable from 'pl-table'
import 'pl-table/themes/index.css' // 引入样式（必须引入)，请查看webpack是否配置了url-loader对woff，ttf文件的引用,不配置会报错哦

import VueJsonp from 'vue-jsonp'
import 'element-ui/lib/theme-chalk/index.css'

import '@/styles/index.scss' // global css
import '@/styles/vehicle.scss' // global css

import '@/views/layout/components/Sidebar/icon/iconfont.css'

import App from './App'
import router from './router'
import store from './store'

import '@/icons' // icon
import './errorLog' // error log
import './permission' // auths control
import * as filters from '@/mixins/filters'
import './dialog.js' // dialog拖拽 拉伸
import './power.js'
// 自定义滚动条
import ScrollBar from './scrollbar.js'
// 对应的css
import 'perfect-scrollbar/css/perfect-scrollbar.css'

import './utils/validate.js'
import Print from 'vue-print-nb'

// 引入百度地图
import BaiduMap from 'vue-baidu-map' // 注册
import echarts from 'echarts'

import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'

import SIdentify from '@/views/login/identify' // 引入vue验证

// import NutUI from '@nutui/nutui'
// import '@nutui/nutui/dist/nutui.css'
import VueQuillEditor from 'vue-quill-editor'// 引入富文本编辑器

import 'quill/dist/quill.core.css'// 富文本

import 'quill/dist/quill.snow.css'// 富文本

import 'quill/dist/quill.bubble.css'// 富文本

Vue.use(VueQuillEditor)// 富文本

// NutUI.install(Vue)

Vue.use(SIdentify)
// use the plugin
// 全局注册过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

const wx = window.wx // index.html中引入外部js，获取js暴露的wx
Vue.$wx = Vue.prototype.$wx = wx

Vue.use(BaiduMap, {
  ak: 'aSqK5pv63yFKXOIpxRIcazoVSmbGiGU9'
})
Vue.prototype.$echarts = echarts

// mock 模拟接口
// import './mock';
Vue.use(VueJsonp)
promise.polyfill()
Vue.use(ElementUI, {
  size: 'small',
  zIndex: 2000
})
window.log = console.log.bind(console)
Vue.config.productionTip = false
Vue.use(Print)
Vue.use(plTable)
Vue.use(Viewer, {
  defaultOptions: {
    'inline': false,
    zIndex: 9999,
    'button': true,
    'navbar': true,
    'title': true,
    'toolbar': {
      zoomIn: {
        show: 1,
        size: 'large'
      },
      zoomOut: {
        show: 1,
        size: 'large'
      },
      oneToOne: 0,
      reset: {
        show: 1,
        size: 'large'
      },
      prev: 4,
      play: {
        show: 4,
        size: 'large'
      },
      next: 4,
      rotateLeft: {
        show: 1,
        size: 'large'
      },
      rotateRight: {
        show: 1,
        size: 'large'
      },
      flipHorizontal: {
        show: 1,
        size: 'large'
      },
      flipVertical: {
        show: 1,
        size: 'large'
      }
    },
    'tooltip': true,
    'movable': true,
    'zoomable': true,
    'rotatable': true,
    'scalable': true,
    'transition': true,
    'fullscreen': true,
    'keyboard': true,
    'url': 'data-source'
  }
})

Vue.directive('fixed-scroll', ScrollBar)

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  router,
  store,
  Print,
  components: {
    App
  },
  data: function() {
    return {
      $COMING_TELE: ''
    }
  },
  template: '<App/>'
})
